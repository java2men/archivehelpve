package ru.admomsk.uikt.types;

import java.util.ArrayList;

public class Individual {
	private String lastName = "";
	private String firstName = "";
	private String middleName = "";
	private String changeLastName = "";
	//Бакыт 14 апреля 2015 года попросила добавить "Дата рождения заявителя"
	private String dateBirth = "";
	
	//Бакыт 9 октября 2014 года попросила вернуть паспортные данные
	private String seriesPassport = "";
	private String numberPassport = "";
	private String whoGivePassport = "";
	private String dateIssuePassport = "";
	
	private String country = "";
	private String region = "";
	private String city = "";

	private String street = "";
	private String home = "";
	private String housing = "";
	private String apartment = "";
	private ArrayList<String> datesBirth;
	private String numberTelephone = "";
	private String email = "";
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLastName() {
		return lastName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getFirstName() {
		return firstName;
	}
	
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getMiddleName() {
		return middleName;
	}
	
	public void setChangeLastName(String changeLastName) {
		this.changeLastName = changeLastName;
	}
	public String getChangeLastName() {
		return changeLastName;
	}
	//Бакыт 14 апреля 2015 года попросила добавить "Дата рождения заявителя"
	public void setDateBirth(String dateBirth) {
		this.dateBirth = dateBirth;
	}
	public String getDateBirth() {
		return dateBirth;
	}
	
	//Бакыт 9 октября 2014 года попросила вернуть паспортные данные
	public void setSeriesPassport(String seriesPassport) {
		this.seriesPassport = seriesPassport;
	}
	public String getSeriesPassport() {
		return seriesPassport;
	}
	
	public void setNumberPassport(String numberPassport) {
		this.numberPassport = numberPassport;
	}
	public String getNumberPassport() {
		return numberPassport;
	}
	
	public void setWhoGivePassport(String whoGivePassport) {
		this.whoGivePassport = whoGivePassport;
	}
	public String getWhoGivePassport() {
		return whoGivePassport;
	}
	
	public void setDateIssuePassport(String dateIssuePassport) {
		this.dateIssuePassport = dateIssuePassport;
	}
	public String getDateIssuePassport() {
		return dateIssuePassport;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountry() {
		return country;
	}
	
	public void setRegion(String region) {
		this.region = region;
	}
	public String getRegion() {
		return region;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	public String getCity() {
		return city;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	public String getStreet() {
		return street;
	}
	
	public void setHome(String home) {
		this.home = home;
	}
	public String getHome() {
		return home;
	}
	
	public void setHousing(String housing) {
		this.housing = housing;
	}
	public String getHousing() {
		return housing;
	}
	
	public void setApartment(String apartment) {
		this.apartment = apartment;
	}
	public String getApartment() {
		return apartment;
	}
	
	public void setDatesBirth(ArrayList<String> datesBirth) {
		this.datesBirth = datesBirth;
	}
	public ArrayList<String> getDatesBirth() {
		return datesBirth;
	}
	
	public void setNumberTelephone(String numberTelephone) {
		this.numberTelephone = numberTelephone;
	}
	public String getNumberTelephone() {
		return numberTelephone;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		return email;
	}
	
	
}
