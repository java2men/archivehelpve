package ru.admomsk.uikt.types;

import java.io.File;
import java.util.ArrayList;

public class RequestContent {
	private String purposeDocumentArchive = "";
	private String whereApplicantWorked = "";
	private ArrayList<String> whatNeededConfirm;
	private String viewArchivedDocuments = "";
	private String infoRequestOrganization = "";
	private String dateStartWork = "";
	private String dateEndWork = "";
	private String dateStartPeriodWages = "";
	private String dateEndPeriodWages = "";
	private String position = "";
	private File requestDir;
	private String otherInformationWork = "";
	
	private String nameSchool = "";
	private String numberSchool = "";
	private String dateStartStudy = "";
	private String dateEndStudy = "";
	private String otherInformationStudy = "";
	
	private String notified60Days = "";
	
	private String type;
	
	public void setPurposeDocumentArchive(String purposeDocumentArchive) {
		this.purposeDocumentArchive = purposeDocumentArchive;
	}
	public String getPurposeDocumentArchive() {
		return purposeDocumentArchive;
	}
	
	public void setWhereApplicantWorked(String whereApplicantWorked) {
		this.whereApplicantWorked = whereApplicantWorked;
	}
	public String getWhereApplicantWorked() {
		return whereApplicantWorked;
	}
	
	public void setWhatNeededConfirm(ArrayList<String> whatNeededConfirm) {
		this.whatNeededConfirm = whatNeededConfirm;
	}
	public ArrayList<String> getWhatNeededConfirm() {
		return whatNeededConfirm;
	}
	
	public void setViewArchivedDocuments(String viewArchivedDocuments) {
		this.viewArchivedDocuments = viewArchivedDocuments;
	}
	public String getViewArchivedDocuments() {
		return viewArchivedDocuments;
	}
	
	public void setInfoRequestOrganization(String infoRequestOrganization) {
		this.infoRequestOrganization = infoRequestOrganization;
	}
	public String getInfoRequestOrganization() {
		return infoRequestOrganization;
	}
	
	public void setDateStartWork(String dateStartWork) {
		this.dateStartWork = dateStartWork;
	}
	public String getDateStartWork() {
		return dateStartWork;
	}
	
	public void setDateEndWork(String dateEndWork) {
		this.dateEndWork = dateEndWork;
	}
	public String getDateEndWork() {
		return dateEndWork;
	}
	
	public void setDateStartPeriodWages(String dateStartPeriodWages) {
		this.dateStartPeriodWages = dateStartPeriodWages;
	}
	public String getDateStartPeriodWages() {
		return dateStartPeriodWages;
	}
	
	public void setDateEndPeriodWages(String dateEndPeriodWages) {
		this.dateEndPeriodWages = dateEndPeriodWages;
	}
	public String getDateEndPeriodWages() {
		return dateEndPeriodWages;
	}
	
	public void setPosition(String position) {
		this.position = position;
	}
	public String getPosition() {
		return position;
	}
	
	public void setRequestDir(File requestDir) {
		this.requestDir = requestDir;
	}
	public File getRequestDir() {
		return requestDir;
	}
	
	public void setOtherInformationWork(String otherInformationWork) {
		this.otherInformationWork = otherInformationWork;
	}
	public String getOtherInformationWork() {
		return otherInformationWork;
	}
	
	public void setNameSchool(String nameSchool) {
		this.nameSchool = nameSchool;
	}
	public String getNameSchool() {
		return nameSchool;
	}
	
	public void setNumberSchool(String numberSchool) {
		this.numberSchool = numberSchool;
	}
	public String getNumberSchool() {
		return numberSchool;
	}
	
	public void setDateStartStudy(String dateStartStudy) {
		this.dateStartStudy = dateStartStudy;
	}
	public String getDateStartStudy() {
		return dateStartStudy;
	}
	
	public void setDateEndStudy(String dateEndStudy) {
		this.dateEndStudy = dateEndStudy;
	}
	public String getDateEndStudy() {
		return dateEndStudy;
	}
	
	public void setOtherInformationStudy(String otherInformationStudy) {
		this.otherInformationStudy = otherInformationStudy;
	}
	public String getOtherInformationStudy() {
		return otherInformationStudy;
	}
	
	public void setNotified60Days(String notified60Days) {
		this.notified60Days = notified60Days;
	}
	public String getNotified60Days() {
		return notified60Days;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	public String getType() {
		return type;
	}
	
}
