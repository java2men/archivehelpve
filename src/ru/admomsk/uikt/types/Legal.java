package ru.admomsk.uikt.types;

import java.util.ArrayList;

public class Legal {
	
	private String fullName = "";
	private String BIN = "";
	private String INN = "";
	private String lastName = "";
	private String firstName = "";
	private String middleName = "";
	private String postHead = "";
	
	private String country = "";
	private String region = "";
	private String city = "";
	private String street = "";
	private String home = "";
	private String housing = "";
	private String apartment = "";

	/*private String mailAddress = "";*/
	private String numberTelephone = "";
	private String email = "";
	
	private String legalIndividualLastName = "";
	private String legalIndividualFirstName = "";
	private String legalIndividualMiddleName = "";
	private String legalIndividualChangeLastName = "";
	private ArrayList<String> legalIndividualDatesBirth;
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getFullName() {
		return fullName;
	}
	
	public void setBIN(String bIN) {
		BIN = bIN;
	}
	public String getBIN() {
		return BIN;
	}
	
	public void setINN(String iNN) {
		INN = iNN;
	}
	public String getINN() {
		return INN;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLastName() {
		return lastName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getFirstName() {
		return firstName;
	}
	
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getMiddleName() {
		return middleName;
	}
	
	public void setPostHead(String postHead) {
		this.postHead = postHead;
	}
	public String getPostHead() {
		return postHead;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountry() {
		return country;
	}
	
	public void setRegion(String region) {
		this.region = region;
	}
	public String getRegion() {
		return region;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	public String getCity() {
		return city;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	public String getStreet() {
		return street;
	}
	
	public void setHome(String home) {
		this.home = home;
	}
	public String getHome() {
		return home;
	}
	
	public void setHousing(String housing) {
		this.housing = housing;
	}
	public String getHousing() {
		return housing;
	}
	
	public void setApartment(String apartment) {
		this.apartment = apartment;
	}
	public String getApartment() {
		return apartment;
	}
	/*
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}
	public String getMailAddress() {
		return mailAddress;
	}
	*/
	public void setNumberTelephone(String numberTelephone) {
		this.numberTelephone = numberTelephone;
	}
	public String getNumberTelephone() {
		return numberTelephone;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		return email;
	}
	
	public void setLegalIndividualLastName(String legalIndividualLastName) {
		this.legalIndividualLastName = legalIndividualLastName;
	}
	public String getLegalIndividualLastName() {
		return legalIndividualLastName;
	}
	
	public void setLegalIndividualFirstName(String legalIndividualFirstName) {
		this.legalIndividualFirstName = legalIndividualFirstName;
	}
	public String getLegalIndividualFirstName() {
		return legalIndividualFirstName;
	}
	
	public void setLegalIndividualMiddleName(String legalIndividualMiddleName) {
		this.legalIndividualMiddleName = legalIndividualMiddleName;
	}
	public String getLegalIndividualMiddleName() {
		return legalIndividualMiddleName;
	}
	
	public void setLegalIndividualChangeLastName(
			String legalIndividualChangeLastName) {
		this.legalIndividualChangeLastName = legalIndividualChangeLastName;
	}
	public String getLegalIndividualChangeLastName() {
		return legalIndividualChangeLastName;
	}
	
	public void setLegalIndividualDatesBirth(
			ArrayList<String> legalIndividualDatesBirth) {
		this.legalIndividualDatesBirth = legalIndividualDatesBirth;
	}
	public ArrayList<String> getLegalIndividualDatesBirth() {
		return legalIndividualDatesBirth;
	}
	
}
