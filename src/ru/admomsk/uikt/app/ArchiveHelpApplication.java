package ru.admomsk.uikt.app;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import ru.admomsk.uikt.types.Individual;
import ru.admomsk.uikt.types.Legal;
import ru.admomsk.uikt.types.RequestContent;
import ru.admomsk.uikt.utils.SendMailUsage;
import ru.admomsk.uikt.view.View;

import com.liferay.portal.kernel.util.FileUtil;
import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2;
import com.vaadin.terminal.gwt.server.PortletApplicationContext2.PortletListener;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class ArchiveHelpApplication extends Application {
	private static boolean reCreateView = false;
	private View view;
	
	@Override
	public void init() {
		createView();
	}
	
	private void createView () {
		view = new View();
		setTheme("archivehelpvetheme");
		setMainWindow(view);
		
		if (getContext() instanceof PortletApplicationContext2) {
			PortletApplicationContext2 ctx =
				(PortletApplicationContext2) getContext();
			ctx.addPortletListener(this, new PortletListener() {
				
				public void handleResourceRequest(ResourceRequest request,
						ResourceResponse response, Window window) {
					// TODO Auto-generated method stub
					
				}
				
				public void handleRenderRequest(RenderRequest request,
						RenderResponse response, Window window) {
					
					//Если reCreateView == true, то пересоздать
					if (reCreateView) {
						view = new View();
						setMainWindow(view);
					}
					PortletSession portletSession = request.getPortletSession();
					view.setRefreshIntervalSession(portletSession.getMaxInactiveInterval()-60);
					view.startRefreshSession();
					reCreateView = true;
				}
				
				public void handleEventRequest(EventRequest request,
						EventResponse response, Window window) {
					// TODO Auto-generated method stub
					
				}
				
				public void handleActionRequest(ActionRequest request,
						ActionResponse response, Window window) {
					// TODO Auto-generated method stub
					
				}
			});
		}
	}
	
	public static void reCreateView() {
		reCreateView = true;
	}
	
	@SuppressWarnings("finally")
	public static boolean sendMessage (RequestContent requestContent, Individual individual, Legal legal) {
		
		//request.xml (создать временно)
		File request = null;
		File requestDir = null;
		File[] filesUpload = null;
		boolean result = false;
		ArrayList<File> attach = null;
		try {
			SendMailUsage sendMailUsage = new SendMailUsage();
			//sendMailUsage.setFrom("ilya.false@gmail.com");
			//sendMailUsage.setFrom("ilya.lozhnikov13@gmail.com");
			//sendMailUsage.setFrom("ialozhnikov@admomsk.ru");
			//sendMailUsage.setFrom("noreply@admomsk.ru");
			sendMailUsage.setTo("ilya.lozhnikov13@gmail.com");
			//sendMailUsage.setTo("ialozhnikov@admomsk.ru");
			//sendMailUsage.setTo("rudakov@admomsk.ru");
			//sendMailUsage.setTo("coosedd@admomsk.ru");
			//sendMailUsage.setTo("cooseddtest@admomsk.ru");
			//стандартный
			//sendMailUsage.setHost("smtp.gmail.com");
			//localhost gmail
			//sendMailUsage.setHost("10.0.0.6");
			//с пробного сервака gmail?
			sendMailUsage.setHost("217.25.215.18");
			//localhost admomsk
			//sendMailUsage.setHost("10.0.0.29");
			//с портала admomsk
			//sendMailUsage.setHost("217.25.215.29");
			//ошибка
			//sendMailUsage.setHost("1.1.1.1");
			
			//sendMailUsage.setUsername("ilya.false");
			//sendMailUsage.setUsername("noreply");
			sendMailUsage.setUsername("Portal");
			//sendMailUsage.setPassword("ialoz");
			sendMailUsage.setSubject("1234");
			//sendMailUsage.setDate(Calendar.getInstance().getTime());
			sendMailUsage.setText("");
			
			//Создать request.xml
			PrintWriter pw = null;
			//создать временную папку для request.xml
			
			if (requestContent.getRequestDir() == null) {
			
				File tempFile = File.createTempFile("archive_help_ve_request", "");
	    		//Получить path временного файла
	    		String pathTempFile = tempFile.getAbsolutePath();
	    		//Удалить временный файл
	    		org.apache.commons.io.FileUtils.forceDelete(tempFile);
	    		//создать директорию на основе path временного файла
	    		requestDir = new File(pathTempFile);
	    		org.apache.commons.io.FileUtils.forceMkdir(requestDir);	            	
				
			} else {
				requestDir = requestContent.getRequestDir();
				filesUpload = requestDir.listFiles();
			}
			
			//создать request.xml
			request = new File(requestDir, "request.xml");
			
			try {
				pw = new PrintWriter(new FileOutputStream(request));
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			
			/*Записать данные request.xml*/
			
			pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
			//пиздец тэг меняем
			pw.println("<ArchivedReference xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"UserRequests.xsd\">");
			//pw.println("<UserQuestion xsi:noNamespaceSchemaLocation=\"UserRequests.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
			pw.println("<notifiedSixtyDays>" + requestContent.getNotified60Days() + "</notifiedSixtyDays>");
			pw.println("<applicantType>"+requestContent.getType()+"</applicantType>");
			
			pw.println("<login>"+individual.getEmail()+"</login>");
			pw.println("<nameLast>"+individual.getLastName()+"</nameLast>");
			pw.println("<nameFirst>"+individual.getFirstName()+"</nameFirst>");
			pw.println("<nameMiddle>"+individual.getMiddleName()+"</nameMiddle>");
			pw.println("<alterName>"+individual.getChangeLastName()+"</alterName>");
			pw.println("<dateBirth>"+individual.getDateBirth()+"</dateBirth>");
			
			//Бакыт 9 октября 2014 года попросила вернуть паспортные данные
			//проверка на пустоту
			if (requestContent.getType().equals("individual")) {
			pw.println("<passport>"+
					individual.getSeriesPassport()+"; "+
					individual.getNumberPassport()+"; "+
					individual.getWhoGivePassport()+"; "+
					individual.getDateIssuePassport()
					+"</passport>");
			} else pw.println("<passport></passport>");
			
			pw.println("<phone>"+individual.getNumberTelephone()+"</phone>");
			pw.println("<country>"+individual.getCountry()+"</country>");
			pw.println("<state></state>");
			pw.println("<region>"+individual.getRegion()+"</region>"); 
			pw.println("<city>"+individual.getCity()+"</city>");
			pw.println("<locality></locality>");
			pw.println("<street>"+individual.getStreet()+"</street>");
			pw.println("<house>"+individual.getHome()+"</house>");
			pw.println("<corp>"+individual.getHousing()+"</corp>");
			pw.println("<flat>"+individual.getApartment()+"</flat>");
			
			ArrayList<String> alDatesBirth = individual.getDatesBirth();
			if (alDatesBirth != null) {
				for (String datesBirth:alDatesBirth) {
					pw.println("<births>"+datesBirth+"</births>");
				}
				//если нет дат, то записать пустой нод
				if (alDatesBirth.size() == 0) pw.println("<births></births>"); 
			} else {
				pw.println("<births></births>");
			}
			
			pw.println("<zipCode></zipCode>");
			pw.println("<section></section>");
			pw.println("<room></room>");
			pw.println("<orgName>"+legal.getFullName()+"</orgName>");
			pw.println("<orgOgrn>"+legal.getBIN()+"</orgOgrn>");
			pw.println("<orgInn>"+legal.getINN()+"</orgInn>");
			pw.println("<orgDirectorLastName>"+legal.getLastName()+"</orgDirectorLastName>");
			pw.println("<orgDirectorLastFirst>"+legal.getFirstName()+"</orgDirectorLastFirst>");
			pw.println("<orgDirectorLastMiddle>"+legal.getMiddleName()+"</orgDirectorLastMiddle>");
			pw.println("<orgDirectorPost>"+legal.getPostHead()+"</orgDirectorPost>");
			
			//проверка на пустоту
			String legalAddress = "";
			//если выбрано юр. лицо
			if (requestContent.getType().equals("legal")) {
				legalAddress = legal.getCountry() + ", ";
				if (!legal.getRegion().equals("")) {
					legalAddress = legalAddress + legal.getRegion() + ", ";
				}
				legalAddress = legalAddress + legal.getCity() + ", ";
				legalAddress = legalAddress + legal.getStreet() + " ";
				legalAddress = legalAddress + legal.getHome();
				if (!legal.getHousing().equals("")) {
					legalAddress = legalAddress + ", корп. "+ legal.getHousing();
				}			
				if (!legal.getApartment().equals("")) {
					legalAddress = legalAddress + ", кв. "+ legal.getApartment();
				}
			}
			pw.println("<orgAddress>"+legalAddress+"</orgAddress>");
			
			/*if (!legal.getCountry().equals("")) {
				pw.println("<orgAddress>"
						+legal.getCountry()+" "
						+legal.getRegion()+" "
						+legal.getCity()+" "
						+legal.getStreet()+" "
						+legal.getHome()+" "
						+legal.getHousing()+" "
						+legal.getApartment()
					+"</orgAddress>");
			} else pw.println("<orgAddress></orgAddress>");*/
			
			/*pw.println("<orgAddress>"+legal.getMailAddress()+"</orgAddress>");*/
			pw.println("<orgTelefon>"+legal.getNumberTelephone()+"</orgTelefon>");
			pw.println("<orgEmail>"+legal.getEmail()+"</orgEmail>");
			
			pw.println("<nameLast>"+legal.getLegalIndividualLastName()+"</nameLast>");
			pw.println("<nameFirst>"+legal.getLegalIndividualFirstName()+"</nameFirst>");
			pw.println("<nameMiddle>"+legal.getLegalIndividualMiddleName()+"</nameMiddle>");
			pw.println("<alterName>"+legal.getLegalIndividualChangeLastName()+"</alterName>");
			
			alDatesBirth = legal.getLegalIndividualDatesBirth();
			if (alDatesBirth != null) {
				for (String datesBirth:alDatesBirth) {
					pw.println("<births>"+datesBirth+"</births>");
				}
				//если нет дат, то записать пустой нод
				if (alDatesBirth.size() == 0) pw.println("<births></births>"); 
			} else {
				pw.println("<births></births>");
			}
			
			pw.println("<purposeRequest>"+requestContent.getPurposeDocumentArchive()+"</purposeRequest>");
			
			String whereApplicantWorked = "";
			if (requestContent.getWhereApplicantWorked() != null) {
				if (requestContent.getWhereApplicantWorked().equals("мунципальная система образования")) whereApplicantWorked = "a";
				if (requestContent.getWhereApplicantWorked().equals("иные ликвидированные, реорганизованные организации города Омска")) whereApplicantWorked = "b";
			}
			pw.println("<workApplicant>"+whereApplicantWorked+"</workApplicant>");
			
			
			//собрать строку из значений Что необходимо подтвердить?*
			boolean factStudy = false;
			String str = "";
			int size = requestContent.getWhatNeededConfirm().size();
			for (int i=0; i<size; i++){
				str = requestContent.getWhatNeededConfirm().get(i);
				if (str.equals("стаж работы;")) {
					pw.println("<neededConfirm>"+"1"+"</neededConfirm>");
				}
				if (str.equals("льготный трудовой стаж работы;")) {
					pw.println("<neededConfirm>"+"2"+"</neededConfirm>");
				}
				if (str.equals("размер заработной платы;")) {
					pw.println("<neededConfirm>"+"3"+"</neededConfirm>");
				}
				if (str.equals("факт переименования, реорганизации, ликвидации организации;")) {
					pw.println("<neededConfirm>"+"4"+"</neededConfirm>");
				}
				if (str.equals("факт обучения в школе.")) {
					pw.println("<neededConfirm>"+"5"+"</neededConfirm>");
					factStudy = true;
				}
				//присоединить пробел если не последний
				//if (i<size-1) str = str.concat(" ");
				//склеить значения
				//whatNeededConfirm = whatNeededConfirm.concat(str);
			}
			//pw.println("<neededConfirm>"+whatNeededConfirm+"</neededConfirm>");
			pw.println("<docArchival>"+requestContent.getViewArchivedDocuments()+"</docArchival>");
			//Если выбран "факт обучения в школе."
			if (factStudy) {
				pw.println("<requestingOrganization>"+requestContent.getNameSchool()+" "+requestContent.getNumberSchool()+"</requestingOrganization>");
				pw.println("<startingWork>"+requestContent.getDateStartStudy()+"</startingWork>");
				pw.println("<completionWork>"+requestContent.getDateEndStudy()+"</completionWork>");
				pw.println("<workPost></workPost>");
				pw.println("<docOther>"+requestContent.getOtherInformationStudy()+"</docOther>");
			} else {
				pw.println("<requestingOrganization>"+requestContent.getInfoRequestOrganization()+"</requestingOrganization>");
				pw.println("<startingWork>"+requestContent.getDateStartWork()+"</startingWork>");
				pw.println("<completionWork>"+requestContent.getDateEndWork()+"</completionWork>");
				pw.println("<startingPeriodWages>"+requestContent.getDateStartPeriodWages()+"</startingPeriodWages>");
				pw.println("<completionPeriodWages>"+requestContent.getDateEndPeriodWages()+"</completionPeriodWages>");
				pw.println("<workPost>"+requestContent.getPosition()+"</workPost>");
				pw.println("<docOther>"+requestContent.getOtherInformationWork()+"</docOther>");
			}
			pw.println("<depCode>61</depCode>");
			pw.println("<depCodeSub>72</depCodeSub>");
			
			if (filesUpload != null) {
				for (File file: filesUpload) {
					pw.println("<file>"+file.getName()+"</file>");
				}
			} else {
				pw.println("<file></file>");
			}
			
			/*File rename = null;
			if (requestContent.getRequestDir() != null) {
				//Расширение с точкой
				String ext = FileUtils.getExt(requestContent.getRequestDir().getName());
				//переименовать файл в новый файл
				rename = new File(requestContent.getRequestDir().getParentFile() + File.separator + "file1" + ext);
				//скопировать в новый переименованный файл
				org.apache.commons.io.FileUtils.copyFile(requestContent.getRequestDir(), rename);
				pw.println("<file>"+rename.getName()+"</file>");
				
			} else {
				pw.println("<file></file>");
			}*/
			
			//пиздец тэг меняем
			pw.println("</ArchivedReference>");
			//pw.println("</UserQuestion>");
			
			pw.close();
			
			//Прикрепить request.xml
			attach = new ArrayList<File>();
			attach.add(request);
			//если есть загруженные файлы то присобачить его к письму
			//if (rename != null) attach.add(rename);
			if (filesUpload != null) {
				attach.addAll(Arrays.asList(filesUpload));
				/*for (File file: filesUpload) {
					attach.add(rename);
				}*/
			}
			sendMailUsage.setFileAsAttachment(attach);
			
			//Послать email
			result = sendMailUsage.sendMessage();
		
		} catch (Exception e) {
			System.out.println("---error sending mail of AcrhiveHelpApplication class---");
			result = false;
			e.printStackTrace();
		} finally {
			
			if (requestDir != null) {
				if (requestDir.exists()) {
					//Удалить папку
					FileUtil.deltree(requestDir);
				}
			}
			
			//проверить еще раз папку и удалить ее в случае необходимости
			try {
				
				if (requestDir != null) {
					if (requestDir.exists()) {
						org.apache.commons.io.FileUtils.deleteDirectory(requestDir);
					}
				}
				
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return result;
		}
	}

}
