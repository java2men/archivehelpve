package ru.admomsk.uikt.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
	
	public static String ONLY_STRING_AND_MINUS_SPACE = "([а-яА-Я-\\u0020]+)";
	public static String ONLY_STRING1 = "([а-яА-Я]+)";
	public static String EMAIL = "^([a-zA-Z0-9_\\.\\-+])+@(([a-zA-Z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+$";
	public static String ONLY_DIGITS = "([01-9]+)";
	
	public static boolean doMatch(String word, String regex) {
		Pattern patternCompile = Pattern.compile(regex);
        Matcher matcher = patternCompile.matcher(word);
        if (matcher.matches()){
        	return true;
        }

        return false;
    }
}
