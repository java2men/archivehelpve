package ru.admomsk.uikt.utils;

import java.util.ArrayList;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;

public class ControlOptionGroup {
	//Коллекция OptionGroup
	private ArrayList<OptionGroup> alOptionGroups = new ArrayList<OptionGroup>();
	//Коллекция Component
	private ArrayList<Component> alComponents = new ArrayList<Component>();
	private int lastSelect = -1;
	
	public ControlOptionGroup() {
		// TODO Auto-generated constructor stub
	}
	
	//Слушатель для OptionGroup 
	private ValueChangeListener vcl = new ValueChangeListener() {
		
		public void valueChange(ValueChangeEvent event) {
			//получить выбраный itemId
			//Integer currentSelect = (Integer) event.getProperty().getValue();
			Integer currentSelect = (Integer)((OptionGroup)((Label)event.getProperty().getValue()).getData()).getData();
			if (currentSelect==null || currentSelect.intValue()==lastSelect) return;
			
			//попробуем быстрее
			//если выбранный элемент не select, то выбрать его и отключить предыдущий
			if (lastSelect!=-1) {
				//alOptionGroups.get(lastSelect).setImmediate(false);
				alOptionGroups.get(lastSelect).unselect(lastSelect);
			}	
			lastSelect=currentSelect;
		}
	};
	
	
	
	public void addValue(OptionGroup radioButtton, Label component) {
		//установить связь с предыдущим radioButton, только если кнопка не первая
		/*if (!alOptionGroups.isEmpty()){
			radioButtton.setParent(alOptionGroups.get(alOptionGroups.size()-1));
		}*/
		alOptionGroups.add(radioButtton);
		Integer item = alOptionGroups.size()-1;
		radioButtton.addItem(new Label());
		radioButtton.setData(alOptionGroups.size()-1);
		radioButtton.setItemCaptionMode(OptionGroup.ITEM_CAPTION_MODE_EXPLICIT);
		radioButtton.setImmediate(true);
		radioButtton.addListener(vcl);
		alComponents.add(component);
		
		//установить связь
		component.setData(radioButtton);
		//radioButtton.setParent(component);
	}
}
