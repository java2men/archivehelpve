package ru.admomsk.uikt.utils;

import java.util.*;
import java.io.*;

import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
import javax.activation.*;
 
public class SendMailUsage {
	
	//адрес получателя сообщения
    private String to = "sendToMailAddress";
    //адрес отправителя сообщения
    private String from = "sendFromMailAddress";
    //SMTP сервер, используемый для отправки
    private String host = "smtpserver.yourisp.net";
    //private int port = 587;
    private String typeServer = "smtp";
    private boolean debug = true;
    private String username = "";
    private String password = "";
    private String subject = "";
    private String text = "";
    private Date date;
    private ArrayList<File> fileAsAttachment = new ArrayList<File>();
	private boolean result;
    public SendMailUsage() {
		// TODO Auto-generated constructor stub
	}
    
	@SuppressWarnings("finally")
	public boolean sendMessage() {
		 
        // Сюда необходимо подставить адрес получателя сообщения
        //String to = "sendToMailAddress";
        //String from = "sendFromMailAddress";
        // Сюда необходимо подставить SMTP сервер, используемый для отправки
        //String host = "smtpserver.yourisp.net";
 
        // Создание свойств, получение сессии
        Properties props = new Properties();
 
        // При использовании статического метода Transport.send()
        // необходимо указать через какой хост будет передано сообщение
        props.put("mail.smtp.host", host);
        // включить аутенфикацию
        props.put("mail.smtps.auth", "true");
        //props.put("mail.smtp.starttls.enable", "true");
        // Включение debug-режима
        //props.put("mail.debug", debug);
 
        // Получение сессии
        Session session = Session.getInstance(props);
        Transport bus = null;
        try {
            // Получение объекта транспорта для передачи электронного сообщения
            bus = session.getTransport(typeServer);
 
            // Устанавливаем соединение один раз
            // Метод Transport.send() отсоединяется после каждой отправки            
            //bus.connect();
            // Обычно для SMTP сервера необходимо указать логин и пароль
            bus.connect(host, username, password);
            
            // Создание объекта сообщения
            Message msg = new MimeMessage(session);
            
            //Установка атрибутов сообщения
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            // Парсинг списка адресов разделённых пробелами. Строгий синтаксис
            msg.setRecipients(Message.RecipientType.CC,
                                InternetAddress.parse(to, true));
            // Парсинг списка адресов разделённых пробелами. Более мягкий синтаксис.
            msg.setRecipients(Message.RecipientType.BCC,
                                InternetAddress.parse(to, false));
 
            msg.setSubject(subject);
            if (date != null) {
            	msg.setSentDate(date);
            }
            
            // Установка контента сообщения и отправка
            
            //если есть файлы для прикрепления, то воспользоваться setFileAsAttachment()
            //System.out.println("fileAsAttachment.size()="+fileAsAttachment.size());
            if (fileAsAttachment.size()>0) {
            	//System.out.println("добавляю файлы");
            	setFileAsAttachment(msg, text, fileAsAttachment);
            } else {
            	//System.out.println("не добавляю файлы");
            	msg.setText(text);
            }
            //применить изменения
            msg.saveChanges();
            //полсать письмо
            bus.sendMessage(msg, address);
 
            bus.close();
            result=true;
        }
        catch (Exception e) {
        	result=false;
            // Печать информации обо всех возможных возникших исключениях
            e.printStackTrace();
            // Получение вложенного исключения
            /*while (mex.getNextException() != null) {
                // Получение следующего исключения в цепочке
                Exception ex = mex.getNextException();
                ex.printStackTrace();
                if (!(ex instanceof MessagingException)) break;
                else mex = (MessagingException)ex;
            }*/
            
            try {
				bus.close();
			} catch (Exception e2) {
				result=false;
				e2.printStackTrace();
			}
            //return false;
        }
        finally {
        	try {
				bus.close();
			} catch (Exception e) {
				result=false;
				e.printStackTrace();
			}
			return result;
        }
        
        //return true;
    }
	
    public void setTo(String to) {
		this.to = to;
	}
    
    public String getTo() {
		return to;
	}
    
	public void setFrom(String from) {
		this.from = from;
	}
	
	public String getFrom() {
		return from;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getHost() {
		return host;
	}

	public void setTypeServer(String typeServer) {
		this.typeServer = typeServer;
	}

	public String getTypeServer() {
		return typeServer;
	}
	
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getDate() {
		return date;
	}

	public void setFileAsAttachment(ArrayList<File> fileAsAttachment) {
		this.fileAsAttachment = fileAsAttachment;
	}

	public ArrayList<File> getFileAsAttachment() {
		return fileAsAttachment;
	}
	
	// Сообщение, состоящее из одной части с типом контента text/plain.
    public static void setTextContent(Message msg) throws MessagingException {
            // Установка типа контента
            String mytxt = "This is a test of sending a " +
                            "plain text e-mail through Java.\n" +
                            "Here is line 2.";
            msg.setText(mytxt);
 
            // Альтернативный способ
            msg.setContent(mytxt, "text/plain");
 
    }
 
    // Сообщение с типом контента multipart/mixed. Обе части имеют тип контента text/plain.
    public static void setMultipartContent(Message msg) throws MessagingException {
        // Создание и заполнение первой части
        MimeBodyPart p1 = new MimeBodyPart();
        p1.setText("This is part one of a test multipart e-mail.");
 
        // Создание и заполнение второй части
        MimeBodyPart p2 = new MimeBodyPart();
        // Here is how to set a charset on textual content
        p2.setText("This is the second part", "us-ascii");
 
        // Создание экземпляра класса Multipart. Добавление частей сообщения в него.
        Multipart mp = new MimeMultipart();
        mp.addBodyPart(p1);
        mp.addBodyPart(p2);
 
        // Установка объекта класса Multipart в качестве контента сообщения
        msg.setContent(mp);
    }
 
    // Прикрепление файла в качестве вложения. Используется JAF FileDataSource.
    public static void setFileAsAttachment(Message msg, String text, ArrayList<File> fileAsAttachment)
             throws MessagingException {

        // Создание экземпляра класса Multipart. Добавление частей сообщения в него.
        Multipart mp = new MimeMultipart();
        
        // Создание и заполнение первой части
        MimeBodyPart p1 = new MimeBodyPart();
        p1.setText(text, "utf-8");

        mp.addBodyPart(p1);
        
        // Создание остальных частей
        MimeBodyPart pn = null;
        // Добавление файла во последующие части часть
        //FileDataSource fds;
        DataSource ds;
        int i=0;
        for (File f:fileAsAttachment) {
        	pn = new MimeBodyPart();
        	//fds = new FileDataSource(f.getPath());
        	//!!!!для xml файла настроить заголовки, он идет первый по счету
    		if (i==0) {
    			ds = new FileDataSource(f) {
    				@Override
    				public String getContentType() {
    				    return "application/xml";
    				}
    				
    				/*@Override
    				public String getName() {
    					System.out.println("super.getName()="+super.getName());
    					return "\""+super.getName()+"\"";
    				}*/
			    };
			    pn.setDataHandler(new DataHandler(ds));
	            pn.setFileName(ds.getName());
    			//pn.addHeaderLine("Content-type!application/xml; name=\"request.xml\"");
    			//pn.addHeaderLine("Content-Disposition!attachment; filename=\"request.xml\"");
    			//pn.set
    			pn.setHeader("Content-Type", "application/xml; name=\"request.xml\"");
    			pn.setHeader("Content-Disposition", "attachment; filename=\"request.xml\"");
    			mp.addBodyPart(pn);
    			i++;
    			continue;
    			//pn.addHeaderLine(arg0);
    		}
    		//ds = new FileDataSource(f.getName());
    		ds = new FileDataSource(f);
    		
			pn.setDataHandler(new DataHandler(ds));
            pn.setFileName(ds.getName());
            mp.addBodyPart(pn);

        	/*pn.setDataHandler(new DataHandler(fds));
            pn.setFileName(fds.getName());*/
        	i++;
        }
 
        //System.out.println("pn.getAllHeaderLines().nextElement().toString()="+pn.getAllHeaderLines().nextElement().toString());
        // Установка экземпляра класса Multipart в качестве контента документа 
        msg.setContent(mp);
        //Удалить якобы не нужный заголовок
        //msg.removeHeader("Content-Transfer-Encoding!base64");
    }
    
    // Добавление в первую часть html-контента.
    // Оптправка данных любого другого типа производится аналогичным образом.
    public static void setHTMLContent(Message msg) throws MessagingException {
 
        String html = "<html><head><title>" +
                        msg.getSubject() +
                        "</title></head><body><h1>" +
                        msg.getSubject() +
                        "</h1><p>This is a test of sending an HTML e-mail" +
                        " through Java.</body></html>";
 
        // HTMLDataSource является внутренним классом
        msg.setDataHandler(new DataHandler(new HTMLDataSource(html)));
    }
 
    /*
     * Внутренний класс работает аналогично JAF datasource и добавляет HTML в контент сообщения
     */
    static class HTMLDataSource implements DataSource {
        private String html;
 
        public HTMLDataSource(String htmlString) {
            html = htmlString;
        }
 
        // Возвращаем html строку в InputStream.
        // Каждый раз возвращается новый поток
        public InputStream getInputStream() throws IOException {
            if (html == null) throw new IOException("Null HTML");
            return new ByteArrayInputStream(html.getBytes());
        }
 
        public OutputStream getOutputStream() throws IOException {
            throw new IOException("This DataHandler cannot write HTML");
        }
 
        public String getContentType() {
            return "text/html";
        }
 
        public String getName() {
            return "JAF text/html dataSource to send e-mail only";
        }
    }
 
}
