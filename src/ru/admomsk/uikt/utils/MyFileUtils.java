package ru.admomsk.uikt.utils;

import java.io.File;

public class MyFileUtils {
	//Получить имя файла без расширения
	public static String getFileName(String filename) {
		//Получить позицию точки в файле
		int dotPos = filename.lastIndexOf(".");
		//Если нет точки, то вернуть имя полностью (значит файл без расширения)
		if (dotPos == -1) return filename; 
		//Имя файла до точки
		return filename.substring(0, dotPos);
	}
	
	//Получить расширение файла с точкой
	public static String getExt(String filename) {
		int dotPos = filename.lastIndexOf(".");
		if (dotPos == -1) return null; 
		//Расширение с точкой
		return filename.substring(dotPos);
	}
	
	//Получить расширение файла с точкой
	public static String getExtNotDot(String filename) {
		int dotPos = filename.lastIndexOf(".");
		if (dotPos == -1) return null; 
		//Расширение с точкой
		return filename.substring(dotPos+1);
	}
	
}
