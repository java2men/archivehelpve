package ru.admomsk.uikt.utils;

import com.vaadin.ui.Component;
import com.vaadin.ui.ProgressIndicator;

public class MyRefresher {

	private ProgressIndicator progressIndicator;
	private int refreshInterval;
	
	public MyRefresher() {
		
		progressIndicator = new ProgressIndicator();
		progressIndicator.setVisible(false);
		//progressIndicator.setImmediate(true);
		progressIndicator.setValue(new Double(0.0));
		progressIndicator.setHeight("0px");
		progressIndicator.setWidth("0px");
	}
	
	public Component create(){
		return progressIndicator;
	}
	
	public Component getComponent() {
		return progressIndicator;
	}
	
	public void setRefreshInterval(int refreshInterval) {
		this.refreshInterval = refreshInterval;
	}

	public int getRefreshInterval() {
		return refreshInterval;
	}
	
	public void startRefresh () {
		progressIndicator.setVisible(true);
		progressIndicator.setPollingInterval(refreshInterval);
	}
	
	public void stopRefresh () {
		progressIndicator.setVisible(false);
		progressIndicator.setPollingInterval(0);
	}
	
	public void oneRefresh () {
		progressIndicator.setPollingInterval(1000);
		progressIndicator.setPollingInterval(0);
	}
	
}
