package ru.admomsk.uikt.view;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import ru.admomsk.uikt.app.ArchiveHelpApplication;
import ru.admomsk.uikt.types.Individual;
import ru.admomsk.uikt.types.Legal;
import ru.admomsk.uikt.types.RequestContent;
import ru.admomsk.uikt.utils.DateUtils;
import ru.admomsk.uikt.utils.MyFileUtils;
import ru.admomsk.uikt.utils.MyRefresher;
import ru.admomsk.uikt.utils.StringUtils;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Field;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class View extends Window {
	private CustomLayout viewLayout;
	private OptionGroup ogPurposeDocumentArchive;
	private TextField tfOtherwise;
	private Label valOgPurposeDocumentArchive;
	private OptionGroup ogWhereApplicantWorked;
	private Label valOgWhereApplicantWorked;
	
	private OptionGroup ogFactWork;
	private CheckBox chWorkExperience;
	private CheckBox chPreferentialWorkExperience;
	private CheckBox chWages;
	private CheckBox chFactChangeOrganization;
	private Label valChWhatNeededConfirm;
	private OptionGroup ogFactStudy;
	
	private CustomLayout culWhereApplicantWorked;
	private OptionGroup ogViewArchivedDocuments;
	private Label valOgViewArchivedDocuments;
	
	private CustomLayout culStep2Work;
	private TextField tfInfoRequestOrganization;
	private Label valTfInfoRequestOrganization;
	private PopupDateField pdfDateStartWork;
	private Label valPdfDateStartWork;
	private PopupDateField pdfDateEndWork;
	private Label valPdfDateEndWork;

	private CustomLayout culPeriodWages;
	private PopupDateField pdfDateStartPeriodWages;
	private Label valPdfDateStartPeriodWages;
	private PopupDateField pdfDateEndPeriodWages;
	private Label valPdfDateEndPeriodWages;
	
	private CustomLayout culPosition;
	private TextField tfPosition;
	private Label valTfPosition;
	private Upload uCopyWorkRecord;
	private Button bCancelUpload;
	private Label valCounterFiles;
	private Label valUCopyWorkRecord;
	private Label filesUCopyWorkRecord;
	private MyReceiver myReceiver;
	private TextField tfOtherInformationWork = null;
	
	private CustomLayout culStep2Study;
	private TextField tfNameSchool;
	private Label valTfNameSchool;
	private TextField tfNumberSchool;
	private Label valTfNumberSchool;
	private PopupDateField pdfDateStartStudy;
	private Label valPdfDateStartStudy;
	private PopupDateField pdfDateEndStudy;
	private Label valPdfDateEndStudy;
	private TextField tfOtherInformationStudy = null;
	
	private OptionGroup ogApplicant;
	private Label valOgApplicant;
	
	private CustomLayout culIndividual;
	private TextField tfIndividualLastName;
	private Label valTfIndividualLastName;
	private TextField tfIndividualFirstName;
	private Label valTfIndividualFirstName;
	private TextField tfIndividualMiddleName;
	//private Label valTfIndividualMiddleName;
	//Бакыт 27 ноября 2015 попросила, чтобы смена фамилии горела всегда
	//private CheckBox cbIndividualChangeLastName;
	private CustomLayout culIndividualChangeLastName;
	private TextField tfIndividualChangeLastName;
	//Бакыт 14 апреля 2015 попросила добавить "Дата рождения завявителя"
	private PopupDateField pdfIndividualDateBirth;
	private Label valPdfIndividualDateBirth;
	
	//Бакыт 9 октября 2014 года попросила вернуть паспортные данные
	private TextField tfIndividualSeriesPassport;
	private Label valTfIndividualSeriesPassport;
	private TextField tfIndividualNumberPassport;
	private Label valTfIndividualNumberPassport;
	private TextField tfIndividualWhoGivePassport;
	private Label valTfIndividualWhoGivePassport;
	private PopupDateField pdfIndividualDateIssuePassport;
	private Label valPdfIndividualDateIssuePassport;
	
	private Upload uCopyPassport;
	private Button bCancelUploadPassport;
	private Label valCounterFilesPassport;
	private Label valUCopyPassport;
	private Label filesUCopyPassport;
	
	private TextField tfIndividualCountry;
	private Label valTfIndividualCountry;
	private TextField tfIndividualRegion;
	/*private Label valTfIndividualRegion;*/
	private TextField tfIndividualCity;
	private Label valTfIndividualCity;
	
	private TextField tfIndividualStreet;
	private Label valTfIndividualStreet;
	private TextField tfIndividualHome;
	private Label valTfIndividualHome;
	private TextField tfIndividualHousing;
	//private Label valTfIndividualHousing;
	private TextField tfIndividualApartment;
	//private Label valTfIndividualApartment;
	
	private CustomLayout culDatesBirth;
	private Label valCssDatesBirth;
	private Button bAddDatesBirth;
	private CssLayout cssDatesBirth;
	private Button bDeleteDatesBirth;
	
	private TextField tfIndividualNumberTelephone;
	private Label valTfIndividualNumberTelephone;
	private TextField tfIndividualEmail;
	private Label valTfIndividualEmail;
	
	private CustomLayout culLegal;
	private TextField tfLegalFullName;
	private Label valTfLegalFullName;
	private TextField tfLegalBIN;
	private Label valTfLegalBIN;
	private TextField tfLegalINN;
	private Label valTfLegalINN;
	private TextField tfLegalLastName;
	private Label valTfLegalLastName;
	private TextField tfLegalFirstName;
	private Label valTfLegalFirstName;
	private TextField tfLegalMiddleName;
	/*private Label valTfLegalMiddleName;*/
	private TextField tfLegalPostHead;
	private Label valTfLegalPostHead;
	
	//Новое
	private CustomLayout culLegalIndividual;
	private TextField tfLegalIndividualLastName;
	private Label valTfLegalIndividualLastName;
	private TextField tfLegalIndividualFirstName;
	private Label valTfLegalIndividualFirstName;
	private TextField tfLegalIndividualMiddleName;
	//private Label valTfIndividualMiddleName;
	//Бакыт 27 ноября 2015 попросила, чтобы смена фамилии горела всегда
	//private CheckBox cbLegalIndividualChangeLastName;
	private TextField tfLegalIndividualChangeLastName;
	private CustomLayout culLegalIndividualChangeLastName;
	private CustomLayout culLegalIndividualDatesBirth;
	private Label valCssLegalIndividualDatesBirth;
	private Button bAddLegalIndividualDatesBirth;
	private CssLayout cssLegalIndividualDatesBirth;
	private Button bDeleteLegalIndividualDatesBirth;
	
	private TextField tfLegalCountry;
	private Label valTfLegalCountry;
	private TextField tfLegalRegion;
	/*private Label valTfLegalRegion;*/
	private TextField tfLegalCity;
	private Label valTfLegalCity;
	private TextField tfLegalStreet;
	private Label valTfLegalStreet;
	private TextField tfLegalHome;
	private Label valTfLegalHome;
	private TextField tfLegalHousing;
	//private Label valTfLegalHousing;
	private TextField tfLegalApartment;
	//private Label valTfLegalApartment;
	
	private TextField tfLegalMailAddress;
	private Label valTfLegalMailAddress;
	private TextField tfLegalNumberTelephone;
	private Label valTfLegalNumberTelephone;
	private TextField tfLegalEmail;
	private Label valTfLegalEmail;
	
	private Button bSend;
	
	private CustomLayout culError;
	private CustomLayout culSent;
	
	private static String fieldRequired = "Поле обязательно для заполнения.";
	private static String incorrectType = "Введенное значение не является датой.";
	private static String emailValidate = "Адрес электронной почты введен некорректно.";
	private static String ogSelectValidate = "Не выбрано значение.";
	private static String fileRequired = "Необходимо прикрепить файл.";
	private FieldRequiredBlurListener fieldRequiredBlurListener;
	private PopupDataFieldRequiredBlurListener popupDataFieldRequiredBlurListener;
	private EmailValidateBlurListener emailValidateBlurListener;
	private OGValueChangeListener ogValueChangeListener;
	
	private RequestContent requestContent;
	private Individual individual;
	private Legal legal;
	/*Бакыт 25 декабря 2015 года попросила добавить эту галочку, ЕЩЕ НЕ РЕАЛИЗОВАННО*/
	//private CheckBox chNotified60Days;
	
	private MyRefresher myRefresher = new MyRefresher();
	private MyRefresher sessionRefresher = new MyRefresher();
	
	private int refreshIntervalSession = 1000*5*60;
	
	public void setRefreshIntervalSession(int sInterval) {
		refreshIntervalSession = sInterval * 1000;
		sessionRefresher.setRefreshInterval(refreshIntervalSession);
	}
	
	public int getRefreshIntervalSession() {
		return refreshIntervalSession/1000;
	}
	
	public void startRefreshSession() {
		sessionRefresher.startRefresh();
	}
	
	public void stopRefreshSession() {
		sessionRefresher.stopRefresh();
	}
	
	public View() {
		// Create the URI fragment utility
	    //final UriFragmentUtility urifu = new UriFragmentUtility();
	    //getWindow().addComponent(urifu);
	    //urifu.setFragment("viweLayout");
		
		viewLayout = buildViewLayout();
		//setContent(viewLayout);
		addComponent(viewLayout);
		
		//Слой для отображения успешной отправки
		culSent = buildSentLayout();
		culSent.setVisible(false);
		addComponent(culSent);
		
		addComponent(sessionRefresher.create());
		sessionRefresher.setRefreshInterval(refreshIntervalSession);
		
	}
	
	//Слушатель для валидации пустого поля
	class FieldRequiredBlurListener implements BlurListener {
		private Field field;
		private Label label;
		
		public FieldRequiredBlurListener(Field field, Label label) {
			this.field = field;
			this.label = label;
			if (this.label.isVisible()) this.label.setVisible(false);
		}
		
		public void blur(BlurEvent event) {
			if (field.getValue() == null || field.getValue().toString().equals("")) {
				label.setValue(fieldRequired);
				label.setVisible(true);
				field.addStyleName("lightError");
			}
			else {
				label.setValue(null);
				label.setVisible(false);
				field.removeStyleName("lightError");
			}
			
		}
		
	}
	
	//Слушатель для валидации пустого поля
	class OGValueChangeListener implements ValueChangeListener {
		private OptionGroup og;
		private Label label;
		
		public OGValueChangeListener(OptionGroup og, Label label) {
			this.og = og;
			this.label = label;
			if (this.label.isVisible()) this.label.setVisible(false);
		}
		
		public void valueChange(ValueChangeEvent event) {
			if (this.label.isVisible()) this.label.setVisible(false);
			this.label.setValue(null);
		}
		
	}
	
	//Слушатель для валидации Email
	class EmailValidateBlurListener implements BlurListener {
		private Field field;
		private Label label;
		
		public EmailValidateBlurListener(Field field, Label label) {
			this.field = field;
			this.label = label;
			if (this.label.isVisible()) this.label.setVisible(false);
		}
		
		public void blur(BlurEvent event) {
			if (field.getValue().toString().length()==0) return;
			if (!StringUtils.doMatch((String)field.getValue(), StringUtils.EMAIL)) {
				label.setValue(emailValidate);
				label.setVisible(true);
				field.addStyleName("lightError");
			}
			else {
				label.setValue(null);
				label.setVisible(false);
				field.removeStyleName("lightError");
			}
			
		}
		
	}
	
	//Слушатель
	class PopupDataFieldRequiredBlurListener implements BlurListener {
		private PopupDateField pdf;
		private Label label;
		
		public PopupDataFieldRequiredBlurListener(PopupDateField pdf, Label label) {
			this.pdf = pdf;
			this.label = label;
			if (this.label.isVisible()) this.label.setVisible(false);
		}
		
		public void blur(BlurEvent event) {
			
			if ((pdf.getValue() == null && pdf.isValid()) /*|| pdf.getValue().toString().equals("")*/) {
				label.setValue(fieldRequired);
				label.setVisible(true);
				pdf.addStyleName("lightError");
			} else {
				if (pdf.isValid()) {
					label.setValue(null);
					label.setVisible(false);
					pdf.removeStyleName("lightError");
				} else {
					label.setValue(incorrectType);
					label.setVisible(true);
					pdf.addStyleName("lightError");
				}
				
			}
			
		}
		
	}
	
	private CustomLayout buildViewLayout() {
		myReceiver = new MyReceiver();
		viewLayout = new CustomLayout("view");
		viewLayout.addStyleName("viewLayout");
		viewLayout.setImmediate(true);
		
		ogPurposeDocumentArchive = new OptionGroup();
		ogPurposeDocumentArchive.addStyleName("ogPurposeDocumentArchive");
		ogPurposeDocumentArchive.setImmediate(true);
		Integer item = new Integer(1);
		ogPurposeDocumentArchive.addItem(item);
		ogPurposeDocumentArchive.setItemCaption(item, "оформление, перерасчет пенсии;");
		item = new Integer(2);
		ogPurposeDocumentArchive.addItem(item);
		ogPurposeDocumentArchive.setItemCaption(item, "оформление пособий;");
		item = new Integer(3);
		ogPurposeDocumentArchive.addItem(item);
		ogPurposeDocumentArchive.setItemCaption(item, "иное:");
		tfOtherwise = new TextField();
		tfOtherwise.setVisible(false);
		tfOtherwise.setWordwrap(true);
		tfOtherwise.setRows(2);
		tfOtherwise.addStyleName("tfOtherwise");
		tfOtherwise.addListener(new BlurListener() {
			
			public void blur(BlurEvent event) {
				//Если ничего не выбрано, то завершить 
				if (ogPurposeDocumentArchive.getValue() == null) return;
				//Если выбранно 3 значение то проверить пустое ли поле иное
				if (ogPurposeDocumentArchive.getValue().equals(new Integer(3))) {
					if (tfOtherwise.getValue().toString().length()==0) {
						valOgPurposeDocumentArchive.setVisible(true);
						valOgPurposeDocumentArchive.setValue(fieldRequired);
						tfOtherwise.addStyleName("lightError");
						tfOtherwise.setData("error");
					}
					else {
						valOgPurposeDocumentArchive.setVisible(false);
						valOgPurposeDocumentArchive.setValue(null);
						tfOtherwise.removeStyleName("lightError");
						tfOtherwise.setData(null);
					}
				}
			}
		});
		viewLayout.addComponent(tfOtherwise, "tfOtherwise");
		//Валидатор
		valOgPurposeDocumentArchive = new Label("",Label.CONTENT_XHTML);
		valOgPurposeDocumentArchive.addStyleName("valOgPurposeDocumentArchive");
		valOgPurposeDocumentArchive.addStyleName("validator");
		valOgPurposeDocumentArchive.setVisible(false);
		viewLayout.addComponent(valOgPurposeDocumentArchive, "valOgPurposeDocumentArchive");
		ogPurposeDocumentArchive.addListener(new ValueChangeListener() {
					
				public void valueChange(ValueChangeEvent event) {
					if (ogPurposeDocumentArchive.isSelected(1) || ogPurposeDocumentArchive.isSelected(2)) tfOtherwise.setVisible(false);
					else {
						tfOtherwise.setVisible(true);
						if (tfOtherwise.getData().equals("error")) {
							valOgPurposeDocumentArchive.setVisible(true);
							valOgPurposeDocumentArchive.setValue(fieldRequired);
							return;
						}
					}
					if (valOgPurposeDocumentArchive.isVisible()) valOgPurposeDocumentArchive.setVisible(false);
					valOgPurposeDocumentArchive.setValue(null);
				}
			}
		);
		viewLayout.addComponent(ogPurposeDocumentArchive, "ogPurposeDocumentArchive");
		
		ogFactWork = new OptionGroup();
		ogFactWork.addItem(1);
		ogFactWork.setItemCaption(1, "факты о работе заявителя:");
		ogFactWork.setImmediate(true);
		ogFactWork.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				if (!ogFactWork.isSelected(1)) return;
				ogFactWork.select(1);
				ogFactStudy.unselect(2);
				valChWhatNeededConfirm.setVisible(false);
				chWorkExperience.setVisible(true);
				chPreferentialWorkExperience.setVisible(true);
				chWages.setVisible(true);
				chFactChangeOrganization.setVisible(true);
				culWhereApplicantWorked.setVisible(true);
				culStep2Work.setVisible(true);
				culStep2Study.setVisible(false);
			}
		});
		viewLayout.addComponent(ogFactWork, "ogFactWork");
		ValueChangeListener vlChWhatNeededConfirm = new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				if (valChWhatNeededConfirm.isVisible()) valChWhatNeededConfirm.setVisible(false);
				valChWhatNeededConfirm.setValue(null);
				
				if (chWorkExperience.booleanValue() == false 
					&& chPreferentialWorkExperience.booleanValue() == false
					&& chWages.booleanValue() == false
					&& chFactChangeOrganization.booleanValue() == false) {
						valChWhatNeededConfirm.setVisible(true);
						valChWhatNeededConfirm.setValue(ogSelectValidate);
				} else {
					valChWhatNeededConfirm.setVisible(false);
					valChWhatNeededConfirm.setValue(null);
				}
				
				//Показывать или скрывать поле Укажите период, за который запрашивается размер заработной платы
				if (chWages.booleanValue()) {
					culPeriodWages.setVisible(true);
				} else {
					culPeriodWages.setVisible(false);
				}
				
				//Показывать или скрывать поле Занимаемая должность
				if (chWorkExperience.booleanValue() 
					|| chPreferentialWorkExperience.booleanValue()
					|| chWages.booleanValue()) {
					culPosition.setVisible(true);
				} else {
					culPosition.setVisible(false);
				}
				
				
				
			}
		};
		chWorkExperience = new CheckBox("стаж работы;");
		chWorkExperience.setVisible(false);
		chWorkExperience.addStyleName("chWorkExperience");
		chWorkExperience.setImmediate(true);
		chWorkExperience.setData(1);
		chWorkExperience.addListener(vlChWhatNeededConfirm);
		viewLayout.addComponent(chWorkExperience, "chWorkExperience");
		chPreferentialWorkExperience = new CheckBox("льготный трудовой стаж работы;");
		chPreferentialWorkExperience.setVisible(false);
		chPreferentialWorkExperience.addStyleName("chPreferentialWorkExperience");
		chPreferentialWorkExperience.setImmediate(true);
		chPreferentialWorkExperience.setData(2);
		chPreferentialWorkExperience.addListener(vlChWhatNeededConfirm);
		//cusDatesBirth
		
		//Бакыт 27 ноября попросила, чтобы даты детей горели всегда, поэтому убираем событие при нажатии льготный трудовой стаж работы;
		/*
		chPreferentialWorkExperience.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				
				culDatesBirth.setVisible(chPreferentialWorkExperience.booleanValue());
				culLegalIndividualDatesBirth.setVisible(chPreferentialWorkExperience.booleanValue());
			}
		});
		*/
		
		viewLayout.addComponent(chPreferentialWorkExperience, "chPreferentialWorkExperience");
		chWages = new CheckBox("размер заработной платы;");
		chWages.setVisible(false);
		chWages.addStyleName("chWages");
		chWages.setImmediate(true);
		chWages.setData(3);
		chWages.addListener(vlChWhatNeededConfirm);
		viewLayout.addComponent(chWages, "chWages");
		chFactChangeOrganization = new CheckBox("факт переименования, реорганизации, ликвидации организации;");
		chFactChangeOrganization.setVisible(false);
		chFactChangeOrganization.addStyleName("chFactChangeOrganization");
		chFactChangeOrganization.setImmediate(true);
		chFactChangeOrganization.setData(4);
		chFactChangeOrganization.addListener(vlChWhatNeededConfirm);
		viewLayout.addComponent(chFactChangeOrganization, "chFactChangeOrganization");
		
		ogFactStudy = new OptionGroup();
		ogFactStudy.addItem(2);
		ogFactStudy.setItemCaption(2, "факт обучения в школе.");
		ogFactStudy.setImmediate(true);
		ogFactStudy.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				if (!ogFactStudy.isSelected(2)) return;
				ogFactStudy.select(2);
				ogFactWork.unselect(1);
				valChWhatNeededConfirm.setVisible(false);
				chWorkExperience.setVisible(false);
				chPreferentialWorkExperience.setVisible(false);
				chWages.setVisible(false);
				chFactChangeOrganization.setVisible(false);
				culWhereApplicantWorked.setVisible(false);
				culStep2Work.setVisible(false);
				culStep2Study.setVisible(true);
			}
		});
		viewLayout.addComponent(ogFactStudy, "ogFactStudy");
		
		//Валидатор
		valChWhatNeededConfirm = new Label("",Label.CONTENT_XHTML);
		valChWhatNeededConfirm.addStyleName("valChWhatNeededConfirm");
		valChWhatNeededConfirm.addStyleName("validator");
		valChWhatNeededConfirm.setVisible(false);
		viewLayout.addComponent(valChWhatNeededConfirm, "valChWhatNeededConfirm");
		
		culWhereApplicantWorked = new CustomLayout("where_applicant_worked");
		culWhereApplicantWorked.setVisible(false);
		ogWhereApplicantWorked = new OptionGroup();
		ogWhereApplicantWorked.addStyleName("ogWhereApplicantWorked");
		ogWhereApplicantWorked.setImmediate(true);
		item = new Integer(1);
		ogWhereApplicantWorked.addItem(item);
		ogWhereApplicantWorked.setItemCaption(item, "мунципальная система образования;");
		item = new Integer(2);
		ogWhereApplicantWorked.addItem(item);
		ogWhereApplicantWorked.setItemCaption(item, "иные ликвидированные, реорганизованные организации города Омска.");
		//Валидатор
		valOgWhereApplicantWorked = new Label("",Label.CONTENT_XHTML);
		valOgWhereApplicantWorked.addStyleName("valOgWhereApplicantWorked");
		valOgWhereApplicantWorked.addStyleName("validator");
		valOgWhereApplicantWorked.setVisible(false);
		culWhereApplicantWorked.addComponent(valOgWhereApplicantWorked, "valOgWhereApplicantWorked");
		ogWhereApplicantWorked.addListener(new ValueChangeListener() {
			
				public void valueChange(ValueChangeEvent event) {
					if (valOgWhereApplicantWorked.isVisible()) valOgWhereApplicantWorked.setVisible(false);
					valOgWhereApplicantWorked.setValue(null);
				}
			}
		);
		culWhereApplicantWorked.addComponent(ogWhereApplicantWorked, "ogWhereApplicantWorked");
		viewLayout.addComponent(culWhereApplicantWorked, "culWhereApplicantWorked");
		
		ogViewArchivedDocuments = new OptionGroup();
		ogViewArchivedDocuments.addStyleName("ogViewArchivedDocuments");
		ogViewArchivedDocuments.setImmediate(true);
		ogViewArchivedDocuments.addItem(new Integer(1));
		ogViewArchivedDocuments.setItemCaption(new Integer(1),"архивная справка;");
		ogViewArchivedDocuments.addItem(new Integer(2));
		ogViewArchivedDocuments.setItemCaption(new Integer(2),"копия архивного документа;");
		ogViewArchivedDocuments.addItem(new Integer(3));
		ogViewArchivedDocuments.setItemCaption(new Integer(3),"архивная выписка.");
		//Валидатор
		valOgViewArchivedDocuments = new Label("",Label.CONTENT_XHTML);
		valOgViewArchivedDocuments.addStyleName("valOgViewArchivedDocuments");
		valOgViewArchivedDocuments.addStyleName("validator");
		valOgViewArchivedDocuments.setVisible(false);
		viewLayout.addComponent(valOgViewArchivedDocuments, "valOgViewArchivedDocuments");
		
		//ogValueChangeListener = new OGValueChangeListener(ogScopeDocument, valOgScopeDocument);
		ogViewArchivedDocuments.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				if (valOgViewArchivedDocuments.isVisible()) valOgViewArchivedDocuments.setVisible(false);
				valOgViewArchivedDocuments.setValue(null);
			}
		});
		viewLayout.addComponent(ogViewArchivedDocuments, "ogViewArchivedDocuments");
		
		culStep2Work = new CustomLayout("step2_work");
		culStep2Work.setVisible(false);
		tfInfoRequestOrganization = new TextField();
		tfInfoRequestOrganization.addStyleName("tfInfoRequestOrganization");
		tfInfoRequestOrganization.setWordwrap(true);
		tfInfoRequestOrganization.setRows(2);
		//tfInfoRequestOrganization.setColumns(20);
		tfInfoRequestOrganization.setImmediate(true);
		//Валидатор
		valTfInfoRequestOrganization = new Label("",Label.CONTENT_XHTML);
		valTfInfoRequestOrganization.addStyleName("valTfInfoRequestOrganization");
		valTfInfoRequestOrganization.addStyleName("validator");
		valTfInfoRequestOrganization.setVisible(false);
		culStep2Work.addComponent(valTfInfoRequestOrganization, "valTfInfoRequestOrganization");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfInfoRequestOrganization, valTfInfoRequestOrganization);
		tfInfoRequestOrganization.addListener(fieldRequiredBlurListener);
		culStep2Work.addComponent(tfInfoRequestOrganization, "tfInfoRequestOrganization");
		
		pdfDateStartWork = new PopupDateField();
		//pdfDateStartWork.setRequired(false);
		pdfDateStartWork.setValidationVisible(false);
		//pdfDateStartWork.setRequiredError("");
		pdfDateStartWork.addStyleName("pdfDateStartWork");
		pdfDateStartWork.setResolution(4);
		valPdfDateStartWork = new Label("",Label.CONTENT_XHTML);
		valPdfDateStartWork.addStyleName("valPdfDateStartWork");
		valPdfDateStartWork.addStyleName("validator");
		valPdfDateStartWork.setVisible(false);
		culStep2Work.addComponent(valPdfDateStartWork, "valPdfDateStartWork");
		popupDataFieldRequiredBlurListener = new PopupDataFieldRequiredBlurListener(pdfDateStartWork, valPdfDateStartWork);
		pdfDateStartWork.addListener(popupDataFieldRequiredBlurListener);
		culStep2Work.addComponent(pdfDateStartWork, "pdfDateStartWork");
		
		pdfDateEndWork = new PopupDateField();
		pdfDateEndWork.setValidationVisible(false);
		pdfDateEndWork.addStyleName("pdfDateEndWork");
		pdfDateEndWork.setResolution(4);
		valPdfDateEndWork = new Label("",Label.CONTENT_XHTML);
		valPdfDateEndWork.addStyleName("valPdfDateEndWork");
		valPdfDateEndWork.addStyleName("validator");
		valPdfDateEndWork.setVisible(false);
		culStep2Work.addComponent(valPdfDateEndWork, "valPdfDateEndWork");
		popupDataFieldRequiredBlurListener = new PopupDataFieldRequiredBlurListener(pdfDateEndWork, valPdfDateEndWork);
		pdfDateEndWork.addListener(popupDataFieldRequiredBlurListener);
		culStep2Work.addComponent(pdfDateEndWork, "pdfDateEndWork");
		
		culPeriodWages = new CustomLayout("period_wages");
		culPeriodWages.addStyleName("culPeriodWages");
		culPeriodWages.setVisible(false);
		pdfDateStartPeriodWages = new PopupDateField();
		pdfDateStartPeriodWages.setValidationVisible(false);
		pdfDateStartPeriodWages.addStyleName("pdfDateStartPeriodWages");
		pdfDateStartPeriodWages.setResolution(4);
		valPdfDateStartPeriodWages = new Label("",Label.CONTENT_XHTML);
		valPdfDateStartPeriodWages.addStyleName("valPdfDateStartPeriodWages");
		valPdfDateStartPeriodWages.addStyleName("validator");
		valPdfDateStartPeriodWages.setVisible(false);
		culPeriodWages.addComponent(valPdfDateStartPeriodWages, "valPdfDateStartPeriodWages");
		popupDataFieldRequiredBlurListener = new PopupDataFieldRequiredBlurListener(pdfDateStartPeriodWages, valPdfDateStartPeriodWages);
		pdfDateStartPeriodWages.addListener(popupDataFieldRequiredBlurListener);
		culPeriodWages.addComponent(pdfDateStartPeriodWages, "pdfDateStartPeriodWages");
		pdfDateEndPeriodWages = new PopupDateField();
		pdfDateEndPeriodWages.setValidationVisible(false);
		pdfDateEndPeriodWages.addStyleName("pdfDateEndPeriodWages");
		pdfDateEndPeriodWages.setResolution(4);
		valPdfDateEndPeriodWages = new Label("",Label.CONTENT_XHTML);
		valPdfDateEndPeriodWages.addStyleName("valPdfDateEndPeriodWages");
		valPdfDateEndPeriodWages.addStyleName("validator");
		valPdfDateEndPeriodWages.setVisible(false);
		culPeriodWages.addComponent(valPdfDateEndPeriodWages, "valPdfDateEndPeriodWages");
		popupDataFieldRequiredBlurListener = new PopupDataFieldRequiredBlurListener(pdfDateEndPeriodWages, valPdfDateEndPeriodWages);
		pdfDateEndPeriodWages.addListener(popupDataFieldRequiredBlurListener);
		culPeriodWages.addComponent(pdfDateEndPeriodWages, "pdfDateEndPeriodWages");
		culStep2Work.addComponent(culPeriodWages, "culPeriodWages");
		
		culPosition = new CustomLayout("position");
		culPosition.addStyleName("culPosition");
		culPosition.setVisible(false);
		tfPosition = new TextField();
		tfPosition.addStyleName("tfPosition");
		tfPosition.setImmediate(true);
		//Валидатор
		valTfPosition = new Label("",Label.CONTENT_XHTML);
		valTfPosition.addStyleName("valTfPosition");
		valTfPosition.addStyleName("validator");
		valTfPosition.setVisible(false);
		culPosition.addComponent(valTfPosition, "valTfPosition");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfPosition, valTfPosition);
		tfPosition.addListener(fieldRequiredBlurListener);
		culPosition.addComponent(tfPosition, "tfPosition");
		culStep2Work.addComponent(culPosition, "culPosition");
		
		/*
		final MyRefresher uploadRefresher = new MyRefresher();
		uploadRefresher.setRefreshInterval(1);
		culStep2Work.addComponent(uploadRefresher.create());
		*/
		
		
		//НЕ РЕАЛИЗОВАНО В СЭД
		/*
		chNotified60Days = new CheckBox("Подтверждаю осведомленность о том, что срок исполнения запроса составляет до 60 дней в соответствии с п. 2 ст. 12 Федерального закона «О порядке рассмотрения обращений граждан Российской Федерации», а также п. 5.10 Правил организации хранения, комплектования, учета и использования документов архивного фонда РФ, утвержденных приказом Министерства культуры РФ от 31.03.2015 № 526.");
		chNotified60Days.setVisible(true);
		chNotified60Days.addStyleName("chNotified60Days");
		chNotified60Days.setImmediate(true);
		ValueChangeListener vlChNotified60Days = new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				//bSend.setEnabled(chNotified60Days.booleanValue());
				bSend.setVisible(chNotified60Days.booleanValue());
				requestContent = new RequestContent();
				if (chNotified60Days.booleanValue()) {
					requestContent.setNotified60Days("true");	
				} else {
					requestContent.setNotified60Days("false");
				}
			}
		};
		chNotified60Days.addListener(vlChNotified60Days);
		viewLayout.addComponent(chNotified60Days, "chNotified60Days");
		*/
		
		
		//MyReceiver receiver = new MyReceiver();
		buildUploaderCopyWorkRecord();
		
		
		
		
		tfOtherInformationWork = new TextField();
		tfOtherInformationWork.setWordwrap(true);
		tfOtherInformationWork.setRows(2);
		tfOtherInformationWork.addStyleName("tfOtherInformationWork");
		culStep2Work.addComponent(tfOtherInformationWork, "tfOtherInformationWork");
		viewLayout.addComponent(culStep2Work, "culStep2Work");
		
		culStep2Study = new CustomLayout("step2_study");
		culStep2Study.setVisible(false);
		tfNameSchool = new TextField();
		tfNameSchool.addStyleName("tfNameSchool");
		//tfNameSchool.setWordwrap(true);
		//tfNameSchool.setRows(2);
		//tfInfoRequestOrganization.setColumns(20);
		tfNameSchool.setImmediate(true);
		//Валидатор
		valTfNameSchool = new Label("",Label.CONTENT_XHTML);
		valTfNameSchool.addStyleName("valTfNameSchool");
		valTfNameSchool.addStyleName("validator");
		valTfNameSchool.setVisible(false);
		culStep2Study.addComponent(valTfNameSchool, "valTfNameSchool");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfNameSchool, valTfNameSchool);
		tfNameSchool.addListener(fieldRequiredBlurListener);
		culStep2Study.addComponent(tfNameSchool, "tfNameSchool");
		
		tfNumberSchool = new TextField();
		tfNumberSchool.addStyleName("tfNumberSchool");
		//tfNumberSchool.setWordwrap(true);
		//tfNumberSchool.setRows(2);
		//tfInfoRequestOrganization.setColumns(20);
		tfNumberSchool.setImmediate(true);
		//Валидатор
		valTfNumberSchool = new Label("",Label.CONTENT_XHTML);
		valTfNumberSchool.addStyleName("valTfNumberSchool");
		valTfNumberSchool.addStyleName("validator");
		valTfNumberSchool.setVisible(false);
		culStep2Study.addComponent(valTfNumberSchool, "valTfNumberSchool");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfNumberSchool, valTfNumberSchool);
		tfNumberSchool.addListener(fieldRequiredBlurListener);
		culStep2Study.addComponent(tfNumberSchool, "tfNumberSchool");
		
		pdfDateStartStudy = new PopupDateField();
		pdfDateStartStudy.setValidationVisible(false);
		pdfDateStartStudy.addStyleName("pdfDateStartStudy");
		pdfDateStartStudy.setResolution(4);
		valPdfDateStartStudy = new Label("",Label.CONTENT_XHTML);
		valPdfDateStartStudy.addStyleName("valPdfDateStartStudy");
		valPdfDateStartStudy.addStyleName("validator");
		valPdfDateStartStudy.setVisible(false);
		culStep2Study.addComponent(valPdfDateStartStudy, "valPdfDateStartStudy");
		popupDataFieldRequiredBlurListener = new PopupDataFieldRequiredBlurListener(pdfDateStartStudy, valPdfDateStartStudy);
		pdfDateStartStudy.addListener(popupDataFieldRequiredBlurListener);
		culStep2Study.addComponent(pdfDateStartStudy, "pdfDateStartStudy");
		
		pdfDateEndStudy = new PopupDateField();
		pdfDateEndStudy.setValidationVisible(false);
		pdfDateEndStudy.addStyleName("pdfDateEndStudy");
		pdfDateEndStudy.setResolution(4);
		valPdfDateEndStudy = new Label("",Label.CONTENT_XHTML);
		valPdfDateEndStudy.addStyleName("valPdfDateEndWork");
		valPdfDateEndStudy.addStyleName("validator");
		valPdfDateEndStudy.setVisible(false);
		culStep2Study.addComponent(valPdfDateEndStudy, "valPdfDateEndStudy");
		popupDataFieldRequiredBlurListener = new PopupDataFieldRequiredBlurListener(pdfDateEndStudy, valPdfDateEndStudy);
		pdfDateEndStudy.addListener(popupDataFieldRequiredBlurListener);
		culStep2Study.addComponent(pdfDateEndStudy, "pdfDateEndStudy");
		tfOtherInformationStudy = new TextField();
		tfOtherInformationStudy.setWordwrap(true);
		tfOtherInformationStudy.setRows(2);
		tfOtherInformationStudy.addStyleName("tfOtherInformationStudy");
		culStep2Study.addComponent(tfOtherInformationStudy, "tfOtherInformationStudy");
		viewLayout.addComponent(culStep2Study, "culStep2Study");
		
		ogApplicant = new OptionGroup();
		ogApplicant.addStyleName("ogApplicant");
		ogApplicant.setImmediate(true);
		item = new Integer(1);
		ogApplicant.addItem(item);
		ogApplicant.setItemCaption(item, "физическое лицо;");
		item = new Integer(2);
		ogApplicant.addItem(item);
		ogApplicant.setItemCaption(item, "юридическое лицо.");
		//Валидатор
		valOgApplicant = new Label("",Label.CONTENT_XHTML);
		valOgApplicant.addStyleName("valOgApplicant");
		valOgApplicant.addStyleName("validator");
		valOgApplicant.setVisible(false);
		viewLayout.addComponent(valOgApplicant, "valOgApplicant");
		ogApplicant.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				
				if (valOgApplicant.isVisible()) valOgApplicant.setVisible(false);
				valOgApplicant.setValue(null);
				
				if (event.getProperty().getValue().equals(1)) {
					culIndividual.setVisible(true);
					culLegal.setVisible(false);
					culLegalIndividual.setVisible(false);
				}
				else {
					culIndividual.setVisible(false);
					culLegal.setVisible(true);
					culLegalIndividual.setVisible(true);
				}
			}
		});
		viewLayout.addComponent(ogApplicant, "ogApplicant");
		
		culIndividual = buildIndividualLayout();
		culIndividual.setVisible(false);
		viewLayout.addComponent(culIndividual, "culIndividual");
		
		culLegal = buildLegalLayout();
		culLegal.setVisible(false);
		viewLayout.addComponent(culLegal, "culLegal");
		
		bSend = new Button("Отправить");
		bSend.addStyleName("bSend");
		bSend.setImmediate(true);
		bSend.setVisible(true);
		bSend.addListener(new ClickListener() {
			
			@SuppressWarnings("unchecked")
			public void buttonClick(ClickEvent event) {
				//bSend.getWindow().setImmediate(true);
				//bSend.getWindow().setEnabled(false);
				
				
				requestContent = new RequestContent();
				individual = new Individual();
				legal = new Legal();
				boolean check = true;
				
				//Если цель получения архивного документа пусто или выбранно 3 значение иное: и оно пусто
				if (ogPurposeDocumentArchive.getValue() == null) {
						valOgPurposeDocumentArchive.setVisible(true);
						valOgPurposeDocumentArchive.setValue(ogSelectValidate);
						check = false;
				} else {
					
					if (ogPurposeDocumentArchive.getValue().equals(new Integer(3))) {
						if (tfOtherwise.getValue().toString().length()==0) {
							valOgPurposeDocumentArchive.setVisible(true);
							valOgPurposeDocumentArchive.setValue(fieldRequired);
							tfOtherwise.addStyleName("lightError");
							tfOtherwise.setData("error");
							check = false;
						} else {
							requestContent.setPurposeDocumentArchive(tfOtherwise.getValue().toString());
							if (check) check = true;
						}
						
					} else {
						String strPurposeDocumentArchive = ogPurposeDocumentArchive.getItemCaption(ogPurposeDocumentArchive.getValue()).replaceAll(";", "").replaceAll("\\.", "");
						requestContent.setPurposeDocumentArchive(strPurposeDocumentArchive);
						if (check) check = true;
					}
					
				}
				
				/*if (ogPurposeDocumentArchive.getValue() == null || 
					(ogPurposeDocumentArchive.getValue().equals(new Integer(3)) 
							&& tfOtherwise.getValue().toString().length()==0)) {
					valOgPurposeDocumentArchive.setVisible(true);
					valOgPurposeDocumentArchive.setValue(fieldRequired);
					check = false;
				}
				else {
					String strPurposeDocumentArchive = null;
					//Получить значение из третьего поля
					if (ogPurposeDocumentArchive.getValue().equals(new Integer(3))) {
						strPurposeDocumentArchive = tfOtherwise.getValue().toString();
					} else {
						strPurposeDocumentArchive = ogPurposeDocumentArchive.getItemCaption(ogPurposeDocumentArchive.getValue()).replaceAll(";", "").replaceAll("\\.", "");
					}
					
					//strScopeDocument = strScopeDocument.replaceAll(".", "");
					requestContent.setPurposeDocumentArchive(strPurposeDocumentArchive);
					if (check) check = true;
				}*/
				//if (!check) System.out.println("Условие1");
				
				//Если выбран факт о работе заявителя
				if (ogFactWork.isSelected(1)) {
					//Проверка Что необходимо подтвердить?
					ArrayList<String> alWhatNeededConfirm = new ArrayList<String>();
					if (chWorkExperience.booleanValue()) alWhatNeededConfirm.add(chWorkExperience.getCaption());
					if (chPreferentialWorkExperience.booleanValue()) alWhatNeededConfirm.add(chPreferentialWorkExperience.getCaption());
					if (chWages.booleanValue()) alWhatNeededConfirm.add(chWages.getCaption());
					if (chFactChangeOrganization.booleanValue()) alWhatNeededConfirm.add(chFactChangeOrganization.getCaption());
					//Если Что необходимо подтвердить? пусто
					if (alWhatNeededConfirm.isEmpty()) {
						valChWhatNeededConfirm.setVisible(true);
						valChWhatNeededConfirm.setValue(ogSelectValidate);
						check = false;
					}
					else {
						requestContent.setWhatNeededConfirm(alWhatNeededConfirm);
						if (check) check = true;
					}
					//if (!check) System.out.println("Условие2");
					
					//Если Где работал заявитель пусто
					if (ogWhereApplicantWorked.getValue() == null) {
						valOgWhereApplicantWorked.setVisible(true);
						valOgWhereApplicantWorked.setValue(ogSelectValidate);
						check = false;
					}
					else {
						String strWhereApplicantWorked = ogWhereApplicantWorked.getItemCaption(ogWhereApplicantWorked.getValue()).replaceAll(";", "").replaceAll("\\.", "");
						//strScopeDocument = strScopeDocument.replaceAll(".", "");
						requestContent.setWhereApplicantWorked(strWhereApplicantWorked);
						if (check) check = true;
					}
					//if (!check) System.out.println("Условие3");
					
					/*Если Сведения о запрашиваемой организации: полное и сокращенное название,
					переименования, ведомственная принадлежность пусто*/
					if (tfInfoRequestOrganization.getValue().toString().length()==0) {
						valTfInfoRequestOrganization.setVisible(true);
						valTfInfoRequestOrganization.setValue(fieldRequired);
						tfInfoRequestOrganization.addStyleName("lightError");
						check = false;
					}
					else {
						requestContent.setInfoRequestOrganization(tfInfoRequestOrganization.getValue().toString());
						if (check) check = true;
					}
					//if (!check) System.out.println("Условие4");
					
					//Если Дата начала трудовой деятельности в организации пусто
					if (pdfDateStartWork.getValue() == null) {
						valPdfDateStartWork.setVisible(true);
						if (pdfDateStartWork.isValid()) valPdfDateStartWork.setValue(fieldRequired);
						else valPdfDateStartWork.setValue(incorrectType);
						pdfDateStartWork.addStyleName("lightError");
						check = false;
					}
					else {
						DateUtils dateStartWork = new DateUtils((Date)pdfDateStartWork.getValue());
						requestContent.setDateStartWork(
							dateStartWork.getStringDay()+"."
							+dateStartWork.getStringMonth()+"."
							+dateStartWork.getStringYear()
						);
						if (check) check = true;
					}
					//if (!check) System.out.println("Условие5");
					
					//Если Дата окончания трудовой деятельности в организации пусто
					if (pdfDateEndWork.getValue() == null) {
						valPdfDateEndWork.setVisible(true);
						if (pdfDateEndWork.isValid()) valPdfDateEndWork.setValue(fieldRequired);
						else valPdfDateEndWork.setValue(incorrectType);
						pdfDateEndWork.addStyleName("lightError");
						check = false;
					}
					else {
						DateUtils dateendWork = new DateUtils((Date)pdfDateEndWork.getValue());
						requestContent.setDateEndWork(
							dateendWork.getStringDay()+"."
							+dateendWork.getStringMonth()+"."
							+dateendWork.getStringYear()
						);
						if (check) check = true;
					}
					
					/*Валидация копии трудовой книжки(стало снова необязательным для заполнения, после того как наехала прокуратура)*/
					/*if ( ((MyReceiver)uCopyWorkRecord.getReceiver()).getReceiveFile() == null ||
					( ((MyReceiver)uCopyWorkRecord.getReceiver()).getReceiveFile() != null && !((MyReceiver)uCopyWorkRecord.getReceiver()).getReceiveFile().exists() ) ) {*/
					/*Валидация копии трудовой книжки(стало обязательным для заполнения)*/
					/*
					valUCopyWorkRecord.setValue("");
					if (((MyReceiver)uCopyWorkRecord.getReceiver()).getCountFilesDirOfReceiveFile() == 0) {
						valUCopyWorkRecord.setVisible(true);
						valUCopyWorkRecord.setValue("<div>" + fileRequired + "</div>");
						uCopyWorkRecord.addStyleName("lightError");
						valUCopyWorkRecord.removeStyleName("loadStatus");
				        valUCopyWorkRecord.removeStyleName("okStatus");
				        valUCopyWorkRecord.addStyleName("validator");
						check = false;
					}
					*/
					
					/*else {
						requestContent.setInfoRequestOrganization(tfInfoRequestOrganization.getValue().toString());
						if (check) check = true;
					}*/
					//if (!check) System.out.println("Условие4");
					
					
					
					//Если размер заработной платы включен
					if (chWages.booleanValue()) {
						//Если Укажите период, за который запрашивается размер заработной платы пусто
						if (pdfDateStartPeriodWages.getValue() == null) {
							valPdfDateStartPeriodWages.setVisible(true);
							if (pdfDateStartPeriodWages.isValid()) valPdfDateStartPeriodWages.setValue(fieldRequired);
							else valPdfDateStartPeriodWages.setValue(incorrectType);
							pdfDateStartPeriodWages.addStyleName("lightError");
							check = false;
						}
						else {
							DateUtils dateStartPeriodWages = new DateUtils((Date)pdfDateStartPeriodWages.getValue());
							requestContent.setDateStartPeriodWages(
								dateStartPeriodWages.getStringDay()+"."
								+dateStartPeriodWages.getStringMonth()+"."
								+dateStartPeriodWages.getStringYear()
							);
							if (check) check = true;
						}
						
						//if (!check) System.out.println("Условие6.1");
						
						
						if (pdfDateEndPeriodWages.getValue() == null) {
							valPdfDateEndPeriodWages.setVisible(true);
							if (pdfDateEndPeriodWages.isValid()) valPdfDateEndPeriodWages.setValue(fieldRequired);
							else valPdfDateEndPeriodWages.setValue(incorrectType);
							pdfDateEndPeriodWages.addStyleName("lightError");
							check = false;
						}
						else {
							DateUtils dateendPeriodWages = new DateUtils((Date)pdfDateEndPeriodWages.getValue());
							requestContent.setDateEndPeriodWages(
								dateendPeriodWages.getStringDay()+"."
								+dateendPeriodWages.getStringMonth()+"."
								+dateendPeriodWages.getStringYear()
							);
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие6.2");
					}
					
					/*Если Занимаемая должность пусто*/
					//проверить выбраны ли значения 1-3 на шаге 1
					if (chWorkExperience.booleanValue() 
							|| chPreferentialWorkExperience.booleanValue() 
							|| chWages.booleanValue()) {
						if (tfPosition.getValue().toString().length()==0) {
							valTfPosition.setVisible(true);
							valTfPosition.setValue(fieldRequired);
							tfPosition.addStyleName("lightError");
							check = false;
						}
						else {
							requestContent.setPosition(tfPosition.getValue().toString());
							if (check) check = true;
						}
					}
					//if (!check) System.out.println("Условие7");
					
					//Установить имя файла
					if (check) {
						//requestContent.setCopyWorkRecord(receiver.getFile());
						requestContent.setRequestDir(((MyReceiver)uCopyWorkRecord.getReceiver()).getDirOfReceiveFile());
					}
					
					//Установить Иные сведения
					if (check) requestContent.setOtherInformationWork(tfOtherInformationWork.getValue().toString());
					
				}
				
				//Если выбран факт об обучении заявителя
				if (ogFactStudy.isSelected(2)) {
					
					//Добавить "факт обучения в школе."
					ArrayList<String> alWhatNeededConfirm = new ArrayList<String>();
					alWhatNeededConfirm.add(ogFactStudy.getItemCaption(2));
					requestContent.setWhatNeededConfirm(alWhatNeededConfirm);
					
					/*Если наименование школы пусто*/
					if (tfNameSchool.getValue().toString().length()==0) {
						valTfNameSchool.setVisible(true);
						valTfNameSchool.setValue(fieldRequired);
						tfNameSchool.addStyleName("lightError");
						check = false;
					}
					else {
						requestContent.setNameSchool(tfNameSchool.getValue().toString());
						if (check) check = true;
					}
					//if (!check) System.out.println("Условие8");
					
					/*Если номер школы пусто*/
					if (tfNumberSchool.getValue().toString().length()==0) {
						valTfNumberSchool.setVisible(true);
						valTfNumberSchool.setValue(fieldRequired);
						tfNumberSchool.addStyleName("lightError");
						check = false;
					}
					else {
						requestContent.setNumberSchool(tfNumberSchool.getValue().toString());
						if (check) check = true;
					}
					//if (!check) System.out.println("Условие9");
					
					//Если Дата начала обучения в школе пусто
					if (pdfDateStartStudy.getValue() == null) {
						valPdfDateStartStudy.setVisible(true);
						if (pdfDateStartStudy.isValid()) valPdfDateStartStudy.setValue(fieldRequired);
						else valPdfDateStartStudy.setValue(incorrectType);
						pdfDateStartStudy.addStyleName("lightError");
						check = false;
					}
					else {
						DateUtils dateStartStudy = new DateUtils((Date)pdfDateStartStudy.getValue());
						requestContent.setDateStartStudy(
							dateStartStudy.getStringDay()+"."
							+dateStartStudy.getStringMonth()+"."
							+dateStartStudy.getStringYear()
						);
						if (check) check = true;
					}
					//if (!check) System.out.println("Условие10");
					
					//Если Дата окончания обучения в школе пусто
					if (pdfDateEndStudy.getValue() == null) {
						valPdfDateEndStudy.setVisible(true);
						if (pdfDateEndStudy.isValid()) valPdfDateEndStudy.setValue(fieldRequired);
						else valPdfDateEndStudy.setValue(incorrectType);
						pdfDateEndStudy.addStyleName("lightError");
						check = false;
					}
					else {
						DateUtils dateendStudy = new DateUtils((Date)pdfDateEndStudy.getValue());
						requestContent.setDateEndStudy(
							dateendStudy.getStringDay()+"."
							+dateendStudy.getStringMonth()+"."
							+dateendStudy.getStringYear()
						);
						if (check) check = true;
					}
					//if (!check) System.out.println("Условие11");
					
					//Установить Иные сведения
					if (check) requestContent.setOtherInformationStudy(tfOtherInformationStudy.getValue().toString());
				}
				
				if (!ogFactWork.isSelected(1) && !ogFactStudy.isSelected(2)){
					valChWhatNeededConfirm.setVisible(true);
					valChWhatNeededConfirm.setValue(ogSelectValidate);
				}
				
				//Если факты заявителя не включены, то показать валидатор
				if (ogFactWork.isSelected(1) && ogFactStudy.isSelected(2)) {
					/*System.out.println("ogFactWork.isSelected(1)="+ogFactWork.isSelected(1));
					System.out.println("ogFactStudy.isSelected(2)"+ogFactStudy.isSelected(2));*/
					valChWhatNeededConfirm.setVisible(true);
					valChWhatNeededConfirm.setValue(ogSelectValidate);
					check = false;
				}
				//if (!check) System.out.println("Условие12");
				
				
				//Если Вид архивного документа пусто
				if (ogViewArchivedDocuments.getValue() == null) {
					valOgViewArchivedDocuments.setVisible(true);
					valOgViewArchivedDocuments.setValue(ogSelectValidate);
					check = false;
				}
				else {
					String strViewArchivedDocuments = ogViewArchivedDocuments.getItemCaption(ogViewArchivedDocuments.getValue()).replaceAll(";", "").replaceAll("\\.", "");
					//strScopeDocument = strScopeDocument.replaceAll(".", "");
					requestContent.setViewArchivedDocuments(strViewArchivedDocuments);
					if (check) check = true;
				}				
				//if (!check) System.out.println("Условие13");
				
				//Если группа радиобутонов "Заявитель" не выбранны
				if (ogApplicant.getValue() == null) {
					valOgApplicant.setVisible(true);
					valOgApplicant.setValue(ogSelectValidate);
					check = false;
				}else {
				
					//Если выбранно физ. лицо
					if (ogApplicant.getValue().equals(1)) {
						
						//Установить тип заявителя
						requestContent.setType("individual");
						
						//Если фамилия пуста
						if (tfIndividualLastName.getValue().toString().length()==0) {
							valTfIndividualLastName.setVisible(true);
							valTfIndividualLastName.setValue(fieldRequired);
							tfIndividualLastName.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setLastName(tfIndividualLastName.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие14");
						
						//Если Имя пусто
						if (tfIndividualFirstName.getValue().toString().length()==0) {
							valTfIndividualFirstName.setVisible(true);
							valTfIndividualFirstName.setValue(fieldRequired);
							tfIndividualFirstName.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setFirstName(tfIndividualFirstName.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие15");
						
						//Если Отчество пусто
						/*if (tfIndividualMiddleName.getValue().toString().length()==0) {
							valTfIndividualMiddleName.setVisible(true);
							valTfIndividualMiddleName.setValue(fieldRequired);
							tfIndividualMiddleName.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setMiddleName(tfIndividualMiddleName.getValue().toString());
							if (check) check = true;
						}*/
						if (tfIndividualMiddleName.getValue() != null) individual.setMiddleName(tfIndividualMiddleName.getValue().toString());
						//if (!check) System.out.println("Условие16");
						
						//Бакыт 27 ноября 2015 попросила, чтобы смена фамилии горела всегда
						/*
						if (cbIndividualChangeLastName.booleanValue() && check) {
							individual.setChangeLastName(tfIndividualChangeLastName.getValue().toString());
						}
						*/
						if (tfIndividualChangeLastName.getValue() != null) individual.setChangeLastName(tfIndividualChangeLastName.getValue().toString());
						
						//Бакыт 14 апреля 2015 года попросила добавить "Дата рождения заявителя"
						//Если Дата выдачи паспорта пусто
						if (pdfIndividualDateBirth.getValue() == null) {
							valPdfIndividualDateBirth.setVisible(true);
							if (pdfIndividualDateBirth.isValid()) valPdfIndividualDateBirth.setValue(fieldRequired);
							else valPdfIndividualDateBirth.setValue(incorrectType);
							pdfIndividualDateBirth.addStyleName("lightError");
							check = false;
						}
						else {
							DateUtils individualDateBirth = new DateUtils((Date)pdfIndividualDateBirth.getValue());
							individual.setDateBirth(
								individualDateBirth.getStringDay()+"."
								+individualDateBirth.getStringMonth()+"."
								+individualDateBirth.getStringYear()
							);
							if (check) check = true;
						}
						
						//Бакыт 9 октября 2014 года попросила вернуть паспортные данные
						//Если Серия паспорта пусто
						if (tfIndividualSeriesPassport.getValue().toString().length()==0) {
							valTfIndividualSeriesPassport.setVisible(true);
							valTfIndividualSeriesPassport.setValue(fieldRequired);
							tfIndividualSeriesPassport.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setSeriesPassport(tfIndividualSeriesPassport.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие17");
						
						//Если Номер паспорта пусто
						if (tfIndividualNumberPassport.getValue().toString().length()==0) {
							valTfIndividualNumberPassport.setVisible(true);
							valTfIndividualNumberPassport.setValue(fieldRequired);
							tfIndividualNumberPassport.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setNumberPassport(tfIndividualNumberPassport.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие18");
						
						//Если Кем выдан паспорт пусто
						if (tfIndividualWhoGivePassport.getValue().toString().length()==0) {
							valTfIndividualWhoGivePassport.setVisible(true);
							valTfIndividualWhoGivePassport.setValue(fieldRequired);
							tfIndividualWhoGivePassport.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setWhoGivePassport(tfIndividualWhoGivePassport.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие19");
						
						//Если Дата выдачи паспорта пусто
						if (pdfIndividualDateIssuePassport.getValue() == null) {
							valPdfIndividualDateIssuePassport.setVisible(true);
							if (pdfIndividualDateIssuePassport.isValid()) valPdfIndividualDateIssuePassport.setValue(fieldRequired);
							else valPdfIndividualDateIssuePassport.setValue(incorrectType);
							pdfIndividualDateIssuePassport.addStyleName("lightError");
							check = false;
						}
						else {
							DateUtils individualDateIssuePassport = new DateUtils((Date)pdfIndividualDateIssuePassport.getValue());
							individual.setDateIssuePassport(
								individualDateIssuePassport.getStringDay()+"."
								+individualDateIssuePassport.getStringMonth()+"."
								+individualDateIssuePassport.getStringYear()
							);
							if (check) check = true;
						}
						
						//if (!check) System.out.println("Условие20");
						
						//Если файл копии паспорта отсутсвует
						if (((ArrayList<File>)uCopyPassport.getData()).size() == 0) {
							
			        		valUCopyPassport.setVisible(true);
							valUCopyPassport.setValue("<div>" + fileRequired + "</div>");
							uCopyPassport.addStyleName("lightError");
							valUCopyPassport.removeStyleName("loadStatus");
					        valUCopyPassport.removeStyleName("okStatus");
					        valUCopyPassport.addStyleName("validator");
							
							check = false;
						}
						else {
							if (check) check = true;
						}
						
						
						//Если Страна пусто
						if (tfIndividualCountry.getValue().toString().length()==0) {
							valTfIndividualCountry.setVisible(true);
							valTfIndividualCountry.setValue(fieldRequired);
							tfIndividualCountry.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setCountry(tfIndividualCountry.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие21");
						//Если Регион пусто
						/*
						if (tfIndividualRegion.getValue().toString().length()==0) {
							valTfIndividualRegion.setVisible(true);
							valTfIndividualRegion.setValue(fieldRequired);
							check = false;
						}
						else {
							individual.setRegion(tfIndividualRegion.getValue().toString());
							if (check) check = true;
						}*/
						if (tfIndividualRegion.getValue() != null) individual.setRegion(tfIndividualRegion.getValue().toString());
						
						//Если Город пусто
						if (tfIndividualCity.getValue().toString().length()==0) {
							valTfIndividualCity.setVisible(true);
							valTfIndividualCity.setValue(fieldRequired);
							tfIndividualCity.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setCity(tfIndividualCity.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие22");
						
						//Если Улица пусто
						if (tfIndividualStreet.getValue().toString().length()==0) {
							valTfIndividualStreet.setVisible(true);
							valTfIndividualStreet.setValue(fieldRequired);
							tfIndividualStreet.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setStreet(tfIndividualStreet.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие23");
						
						//Если Дом пусто
						if (tfIndividualHome.getValue().toString().length()==0) {
							valTfIndividualHome.setVisible(true);
							valTfIndividualHome.setValue(fieldRequired);
							tfIndividualHome.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setHome(tfIndividualHome.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие24");
						//Если Корпус пусто
						/*
						if (tfIndividualHousing.getValue().toString().length()==0) {
							valTfIndividualHousing.setVisible(true);
							valTfIndividualHousing.setValue(fieldRequired);
							check = false;
						}
						else {
							individual.setHousing(tfIndividualHousing.getValue().toString());
							if (check) check = true;
						}
						*/
						if (tfIndividualHousing.getValue() != null) individual.setHousing(tfIndividualHousing.getValue().toString());
						
						//Если Квартира пусто
						/*
						if (tfIndividualApartment.getValue().toString().length()==0) {
							valTfIndividualApartment.setVisible(true);
							valTfIndividualApartment.setValue(fieldRequired);
							check = false;
						}
						else {
							individual.setApartment(tfIndividualApartment.getValue().toString());
							if (check) check = true;
						}
						*/
						if (tfIndividualApartment.getValue() != null) individual.setApartment(tfIndividualApartment.getValue().toString());
						
						//если льготный стаж работы включен, то заполнить дни рождения детей
						//Бакыт 27 ноября 2015 года попросила исключить данное условие 
						//if (chPreferentialWorkExperience.booleanValue()) {
						if (true) {
							Iterator itr = cssDatesBirth.getComponentIterator();
							ArrayList<String> alDatesBirth = new ArrayList<String>();
							while (itr.hasNext()) {
								
								Date date = (Date)((PopupDateField)itr.next()).getValue();
								if (date != null) {
									DateUtils duDatesBirth = new DateUtils(date);
									alDatesBirth.add(
										duDatesBirth.getStringDay()+"."
										+duDatesBirth.getStringMonth()+"."
										+duDatesBirth.getStringYear()
									);
								}
								
							}
							individual.setDatesBirth(alDatesBirth);
						}
						
						//Если Номер телефона пусто
						if (tfIndividualNumberTelephone.getValue().toString().length()==0) {
							valTfIndividualNumberTelephone.setVisible(true);
							valTfIndividualNumberTelephone.setValue(fieldRequired);
							tfIndividualNumberTelephone.addStyleName("lightError");
							check = false;
						}
						else {
							individual.setNumberTelephone(tfIndividualNumberTelephone.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие25");
						
						//Если Адрес электронной почты пусто
						if (tfIndividualEmail.getValue().toString().length()==0) {
							valTfIndividualEmail.setVisible(true);
							valTfIndividualEmail.setValue(fieldRequired);
							tfIndividualEmail.addStyleName("lightError");
							check = false;
						}
						else {
							//Если Адрес электронной почты не валиден							
							if (!StringUtils.doMatch(tfIndividualEmail.getValue().toString(), StringUtils.EMAIL)) {
								valTfIndividualEmail.setVisible(true);
								valTfIndividualEmail.setValue(emailValidate);
								tfIndividualEmail.addStyleName("lightError");
								check = false;
							}
							else {
								individual.setEmail(tfIndividualEmail.getValue().toString());
								if (check) check = true;
							}
							//if (!check) System.out.println("Условие26");
						}
						//if (!check) System.out.println("Условие27");
					}
					//Если выбранно юр. лицо
					else {
						
						//Установить тип заявителя
						requestContent.setType("legal");
						
						//Если Полное наименование пусто
						if (tfLegalFullName.getValue().toString().length()==0) {
							valTfLegalFullName.setVisible(true);
							valTfLegalFullName.setValue(fieldRequired);
							tfLegalFullName.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setFullName(tfLegalFullName.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие28");
						
						//Если ОГРН пусто
						if (tfLegalBIN.getValue().toString().length()==0) {
							valTfLegalBIN.setVisible(true);
							valTfLegalBIN.setValue(fieldRequired);
							tfLegalBIN.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setBIN(tfLegalBIN.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие29");
						
						//Если ИНН пусто
						if (tfLegalINN.getValue().toString().length()==0) {
							valTfLegalINN.setVisible(true);
							valTfLegalINN.setValue(fieldRequired);
							tfLegalINN.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setINN(tfLegalINN.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие30");
						
						//Если Фамилия пуста
						if (tfLegalLastName.getValue().toString().length()==0) {
							valTfLegalLastName.setVisible(true);
							valTfLegalLastName.setValue(fieldRequired);
							tfLegalLastName.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setLastName(tfLegalLastName.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие31");
						
						//Если Имя пусто
						if (tfLegalFirstName.getValue().toString().length()==0) {
							valTfLegalFirstName.setVisible(true);
							valTfLegalFirstName.setValue(fieldRequired);
							tfLegalFirstName.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setFirstName(tfLegalFirstName.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие32");
						
						//Если Отчество пусто
						/*if (tfLegalMiddleName.getValue().toString().length()==0) {
							valTfLegalMiddleName.setVisible(true);
							valTfLegalMiddleName.setValue(fieldRequired);
							tfLegalMiddleName.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setMiddleName(tfLegalMiddleName.getValue().toString());
							if (check) check = true;
						}*/
						if (tfLegalMiddleName.getValue() != null) legal.setMiddleName(tfLegalMiddleName.getValue().toString());
						//if (!check) System.out.println("Условие33");
						
						//Если Должность руководителя пусто
						if (tfLegalPostHead.getValue().toString().length()==0) {
							valTfLegalPostHead.setVisible(true);
							valTfLegalPostHead.setValue(fieldRequired);
							tfLegalPostHead.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setPostHead(tfLegalPostHead.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие34");
						
						//Если Страна пусто
						if (tfLegalCountry.getValue().toString().length()==0) {
							valTfLegalCountry.setVisible(true);
							valTfLegalCountry.setValue(fieldRequired);
							tfLegalCountry.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setCountry(tfLegalCountry.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие35");
						
						//Если Регион пусто
						/*
						if (tfLegalRegion.getValue().toString().length()==0) {
							valTfLegalRegion.setVisible(true);
							valTfLegalRegion.setValue(fieldRequired);
							check = false;
						}
						else {
							legal.setRegion(tfLegalRegion.getValue().toString());
							if (check) check = true;
						}
						*/
						if (check) legal.setRegion(tfLegalRegion.getValue().toString());

						//Если Город пусто
						if (tfLegalCity.getValue().toString().length()==0) {
							valTfLegalCity.setVisible(true);
							valTfLegalCity.setValue(fieldRequired);
							tfLegalCity.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setCity(tfLegalCity.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие36");
						
						//Если Улица пусто
						if (tfLegalStreet.getValue().toString().length()==0) {
							valTfLegalStreet.setVisible(true);
							valTfLegalStreet.setValue(fieldRequired);
							tfLegalStreet.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setStreet(tfLegalStreet.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие37");
						
						//Если Дом пусто
						if (tfLegalHome.getValue().toString().length()==0) {
							valTfLegalHome.setVisible(true);
							valTfLegalHome.setValue(fieldRequired);
							tfLegalHome.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setHome(tfLegalHome.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие38");
						
						//Если Корпус пусто
						/*
						if (tfLegalHousing.getValue().toString().length()==0) {
							valTfLegalHousing.setVisible(true);
							valTfLegalHousing.setValue(fieldRequired);
							check = false;
						}
						else {
							legal.setHousing(tfLegalHousing.getValue().toString());
							if (check) check = true;
						}
						*/
						if (tfLegalHousing.getValue() != null) legal.setHousing(tfLegalHousing.getValue().toString());
						//if (!check) System.out.println("Условие39");
						
						//Если Квартира пусто
						/*
						if (tfLegalApartment.getValue().toString().length()==0) {
							valTfLegalApartment.setVisible(true);
							valTfLegalApartment.setValue(fieldRequired);
							check = false;
						}
						else {
							legal.setApartment(tfLegalApartment.getValue().toString());
							if (check) check = true;
						}
						*/
						if (tfLegalApartment.getValue() != null) legal.setApartment(tfLegalApartment.getValue().toString());
						//if (!check) System.out.println("Условие40");
						
						//Если Почтовый адрес пусто
						/*
						if (tfLegalMailAddress.getValue().toString().length()==0) {
							valTfLegalMailAddress.setVisible(true);
							valTfLegalMailAddress.setValue(fieldRequired);
							check = false;
						}
						else {
							legal.setMailAddress(tfLegalMailAddress.getValue().toString());
							if (check) check = true;
						}
						if (!check) System.out.println("Условие41");
						*/
						
						//Если Номер телефона пусто
						if (tfLegalNumberTelephone.getValue().toString().length()==0) {
							valTfLegalNumberTelephone.setVisible(true);
							valTfLegalNumberTelephone.setValue(fieldRequired);
							tfLegalNumberTelephone.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setNumberTelephone(tfLegalNumberTelephone.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие42");
						
						//Если Адрес электронной почты пусто
						if (tfLegalEmail.getValue().toString().length()==0) {
							valTfLegalEmail.setVisible(true);
							valTfLegalEmail.setValue(fieldRequired);
							tfLegalEmail.addStyleName("lightError");
							check = false;
						}
						else {
							//Если Адрес электронной почты не валиден							
							if (!StringUtils.doMatch(tfLegalEmail.getValue().toString(), StringUtils.EMAIL)) {
								valTfLegalEmail.setVisible(true);
								valTfLegalEmail.setValue(emailValidate);
								tfLegalEmail.addStyleName("lightError");
								check = false;
							}
							else {
								legal.setEmail(tfLegalEmail.getValue().toString());
								if (check) check = true;
							}
							//if (!check) System.out.println("Условие43");
						}
						//if (!check) System.out.println("Условие44");
						
						//Если фамилия физ. лица от юр. лица пуста
						if (tfLegalIndividualLastName.getValue().toString().length()==0) {
							valTfLegalIndividualLastName.setVisible(true);
							valTfLegalIndividualLastName.setValue(fieldRequired);
							tfLegalIndividualLastName.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setLegalIndividualLastName(tfLegalIndividualLastName.getValue().toString());
							if (check) check = true;
						}
						
						//Если имя физ. лица от юр. лица пуста
						if (tfLegalIndividualFirstName.getValue().toString().length()==0) {
							valTfLegalIndividualFirstName.setVisible(true);
							valTfLegalIndividualFirstName.setValue(fieldRequired);
							tfLegalIndividualFirstName.addStyleName("lightError");
							check = false;
						}
						else {
							legal.setLegalIndividualFirstName(tfLegalIndividualFirstName.getValue().toString());
							if (check) check = true;
						}
						//if (!check) System.out.println("Условие45");
						
						//Если отчество физ. лица от юр. лица пуста
						/*if (tfLegalIndividualMiddleName.getValue().toString().length()==0) {
							valTfLegalIndividualMiddleName.setVisible(true);
							valTfLegalIndividualMiddleName.setValue(fieldRequired);
							tfLegalIndividualMiddleName.addStyleName("lightError");
							check = false;
						}
						else {
							LegalIndividual.setMiddleName(tfLegalIndividualMiddleName.getValue().toString());
							if (check) check = true;
						}*/
						if (tfLegalIndividualMiddleName.getValue() != null) legal.setLegalIndividualMiddleName(tfLegalIndividualMiddleName.getValue().toString());
						
						//Бакыт 27 ноября 2015 попросила, чтобы смена фамилии горела всегда
						/*
						if (cbLegalIndividualChangeLastName.booleanValue()) {
							legal.setLegalIndividualChangeLastName(tfLegalIndividualChangeLastName.getValue().toString());
						}
						*/
						if (tfLegalIndividualChangeLastName.getValue() != null) legal.setLegalIndividualChangeLastName(tfLegalIndividualChangeLastName.getValue().toString()); 
						
						
						//если льготный стаж работы включен, то заполнить дни рождения детей
						//Бакыт 27 ноября 2015 года попросила убрать данное условие
						//if (chPreferentialWorkExperience.booleanValue()) {
						if (true) {
							Iterator itr = cssLegalIndividualDatesBirth.getComponentIterator();
							ArrayList<String> alDatesBirth = new ArrayList<String>();
							while (itr.hasNext()) {
								
								Date date = (Date)((PopupDateField)itr.next()).getValue();
								if (date != null) {
									DateUtils duDatesBirth = new DateUtils(date);
									alDatesBirth.add(
										duDatesBirth.getStringDay()+"."
										+duDatesBirth.getStringMonth()+"."
										+duDatesBirth.getStringYear()
									);
								}
								
							}
							legal.setLegalIndividualDatesBirth(alDatesBirth);
						}
						//if (!check) System.out.println("Условие46");
						
					}
				}
				
				//Отправить письмо
				//System.out.println("check="+check);
				//check=true;
				//если все данные заполенны
				if (check) {
					
					getWindow().addComponent(myRefresher.create());
					myRefresher.setRefreshInterval(1000);
					myRefresher.startRefresh();
					viewLayout.setEnabled(false);
					
					new Thread(new Runnable() {
						
						public void run() {
							/*если письмо отправлено, установить
							слой отображающий успешности отправки*/
							if (ArchiveHelpApplication.sendMessage(requestContent, individual, legal)) {
								//setContent(culSent);
								viewLayout.setVisible(false);
								culSent.setVisible(true);
								//Разрешить пересоздать View, а-ля почистить сессию
								ArchiveHelpApplication.reCreateView();
							}
							/*если письмо не отправлено, 
							установить сделать видимым слой отображающий ошибку отправки*/
							else {
								culError.setVisible(true);
							}
							
				            synchronized (getApplication()) {
				            	viewLayout.setEnabled(true);
				            }
				            myRefresher.stopRefresh();
						}
						
					}).start();
					
					
				}
				
			}
		});
		viewLayout.addComponent(bSend, "bSend");
		
		//Слой для отображения ошибки
		culError = buildErrorLayout();
		culError.setVisible(false);
		viewLayout.addComponent(culError, "culError");
		
		return viewLayout;
	}
	
	private CustomLayout buildIndividualLayout() {
		culIndividual = new CustomLayout("individual");
		culIndividual.addStyleName("culIndividual");
		
		tfIndividualLastName = new TextField();
		tfIndividualLastName.addStyleName("tfIndividualLastName");
		valTfIndividualLastName = new Label("",Label.CONTENT_XHTML);
		valTfIndividualLastName.addStyleName("valTfIndividualLastName");
		valTfIndividualLastName.addStyleName("validator");
		valTfIndividualLastName.setVisible(true);
		culIndividual.addComponent(valTfIndividualLastName, "valTfIndividualLastName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualLastName, valTfIndividualLastName);
		tfIndividualLastName.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualLastName, "tfIndividualLastName");
		
		tfIndividualFirstName = new TextField();
		tfIndividualFirstName.addStyleName("tfIndividualFirstName");
		valTfIndividualFirstName = new Label("",Label.CONTENT_XHTML);
		valTfIndividualFirstName.addStyleName("valTfIndividualFirstName");
		valTfIndividualFirstName.addStyleName("validator");
		valTfIndividualFirstName.setVisible(false);
		culIndividual.addComponent(valTfIndividualFirstName, "valTfIndividualFirstName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualFirstName, valTfIndividualFirstName);
		tfIndividualFirstName.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualFirstName, "tfIndividualFirstName");
		
		tfIndividualMiddleName = new TextField();
		tfIndividualMiddleName.addStyleName("tfIndividualMiddleName");
		/*valTfIndividualMiddleName = new Label("",Label.CONTENT_XHTML);
		valTfIndividualMiddleName.addStyleName("valTfIndividualMiddleName");
		valTfIndividualMiddleName.addStyleName("validator");
		valTfIndividualMiddleName.setVisible(false);
		culIndividual.addComponent(valTfIndividualMiddleName, "valTfIndividualMiddleName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualMiddleName, valTfIndividualMiddleName);
		tfIndividualMiddleName.addListener(fieldRequiredBlurListener);
		*/
		culIndividual.addComponent(tfIndividualMiddleName, "tfIndividualMiddleName");
		
		/*
		cbIndividualChangeLastName = new CheckBox();
		cbIndividualChangeLastName.addStyleName("cbIndividualChangeLastName");
		cbIndividualChangeLastName.setImmediate(true);
		cbIndividualChangeLastName.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				culIndividualChangeLastName.setVisible(cbIndividualChangeLastName.booleanValue());
			}
		});
		culIndividual.addComponent(cbIndividualChangeLastName, "cbIndividualChangeLastName");
		*/
		
		culIndividualChangeLastName = new CustomLayout("change_lastname");
		//culIndividualChangeLastName.setVisible(false);
		culIndividualChangeLastName.setVisible(true);
		tfIndividualChangeLastName = new TextField();
		tfIndividualChangeLastName.addStyleName("tfIndividualChangeLastName");
		//установить ширину
		tfIndividualChangeLastName.setWidth("57%");
		culIndividualChangeLastName.addComponent(tfIndividualChangeLastName, "tfIndividualChangeLastName");
		culIndividual.addComponent(culIndividualChangeLastName, "culIndividualChangeLastName");
		
		//Бакыт 14 апреля 2015 года попросила добавить "Дата рождения заявителя"
		pdfIndividualDateBirth = new PopupDateField();
		
		pdfIndividualDateBirth.setValidationVisible(false);
		pdfIndividualDateBirth.addStyleName("pdfIndividualDateBirth");
		pdfIndividualDateBirth.setResolution(4);
		pdfIndividualDateBirth.setRequired(false);
		valPdfIndividualDateBirth = new Label("",Label.CONTENT_XHTML);
		valPdfIndividualDateBirth.addStyleName("valPdfIndividualDateBirth");
		valPdfIndividualDateBirth.addStyleName("validator");
		valPdfIndividualDateBirth.setVisible(false);
		culIndividual.addComponent(valPdfIndividualDateBirth, "valPdfIndividualDateBirth");
		popupDataFieldRequiredBlurListener = new PopupDataFieldRequiredBlurListener(pdfIndividualDateBirth, valPdfIndividualDateBirth);
		pdfIndividualDateBirth.addListener(popupDataFieldRequiredBlurListener);
		culIndividual.addComponent(pdfIndividualDateBirth, "pdfIndividualDateBirth");
		
		//Бакыт 9 октября 2014 года попросила вернуть паспортные данные
		tfIndividualSeriesPassport = new TextField();
		tfIndividualSeriesPassport.addStyleName("tfIndividualSeriesPassport");
		valTfIndividualSeriesPassport = new Label("",Label.CONTENT_XHTML);
		valTfIndividualSeriesPassport.addStyleName("valTfIndividualSeriesPassport");
		valTfIndividualSeriesPassport.addStyleName("validator");
		valTfIndividualSeriesPassport.setVisible(false);
		culIndividual.addComponent(valTfIndividualSeriesPassport, "valTfIndividualSeriesPassport");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualSeriesPassport, valTfIndividualSeriesPassport);
		tfIndividualSeriesPassport.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualSeriesPassport, "tfIndividualSeriesPassport");
		
		tfIndividualNumberPassport = new TextField();
		tfIndividualNumberPassport.addStyleName("tfIndividualNumberPassport");
		valTfIndividualNumberPassport = new Label("",Label.CONTENT_XHTML);
		valTfIndividualNumberPassport.addStyleName("valTfIndividualNumberPassport");
		valTfIndividualNumberPassport.addStyleName("validator");
		valTfIndividualNumberPassport.setVisible(false);
		culIndividual.addComponent(valTfIndividualNumberPassport, "valTfIndividualNumberPassport");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualNumberPassport, valTfIndividualNumberPassport);
		tfIndividualNumberPassport.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualNumberPassport, "tfIndividualNumberPassport");
		
		tfIndividualWhoGivePassport = new TextField();
		tfIndividualWhoGivePassport.setWordwrap(true);
		tfIndividualWhoGivePassport.setRows(2);
		tfIndividualWhoGivePassport.addStyleName("tfIndividualWhoGivePassport");
		valTfIndividualWhoGivePassport = new Label("",Label.CONTENT_XHTML);
		valTfIndividualWhoGivePassport.addStyleName("valTfIndividualWhoGivePassport");
		valTfIndividualWhoGivePassport.addStyleName("validator");
		valTfIndividualWhoGivePassport.setVisible(false);
		culIndividual.addComponent(valTfIndividualWhoGivePassport, "valTfIndividualWhoGivePassport");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualWhoGivePassport, valTfIndividualWhoGivePassport);
		tfIndividualWhoGivePassport.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualWhoGivePassport, "tfIndividualWhoGivePassport");
		
		pdfIndividualDateIssuePassport = new PopupDateField();
		
		pdfIndividualDateIssuePassport.setValidationVisible(false);
		pdfIndividualDateIssuePassport.addStyleName("pdfIndividualDateIssuePassport");
		pdfIndividualDateIssuePassport.setResolution(4);
		pdfIndividualDateIssuePassport.setRequired(false);
		valPdfIndividualDateIssuePassport = new Label("",Label.CONTENT_XHTML);
		valPdfIndividualDateIssuePassport.addStyleName("valPdfIndividualDateIssuePassport");
		valPdfIndividualDateIssuePassport.addStyleName("validator");
		valPdfIndividualDateIssuePassport.setVisible(false);
		culIndividual.addComponent(valPdfIndividualDateIssuePassport, "valPdfIndividualDateIssuePassport");
		popupDataFieldRequiredBlurListener = new PopupDataFieldRequiredBlurListener(pdfIndividualDateIssuePassport, valPdfIndividualDateIssuePassport);
		pdfIndividualDateIssuePassport.addListener(popupDataFieldRequiredBlurListener);
		culIndividual.addComponent(pdfIndividualDateIssuePassport, "pdfIndividualDateIssuePassport");
		
		//31 марта 2017 года попросили добавить возможность прикреплять копию паспорта
		//culIndividual.addComponent(uCopyWorkRecord, "uCopyWorkRecord");
		buildUploaderPassport();
		
		
		tfIndividualCountry = new TextField();
		tfIndividualCountry.setValue("Россия");
		tfIndividualCountry.addStyleName("tfIndividualCountry");
		valTfIndividualCountry = new Label("",Label.CONTENT_XHTML);
		valTfIndividualCountry.addStyleName("valTfIndividualCountry");
		valTfIndividualCountry.addStyleName("validator");
		valTfIndividualCountry.setVisible(false);
		culIndividual.addComponent(valTfIndividualCountry, "valTfIndividualCountry");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualCountry, valTfIndividualCountry);
		tfIndividualCountry.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualCountry, "tfIndividualCountry");
		
		tfIndividualRegion = new TextField();
		tfIndividualRegion.setValue("Омская область");
		tfIndividualRegion.addStyleName("tfIndividualRegion");
		/*
		valTfIndividualRegion = new Label("",Label.CONTENT_XHTML);
		valTfIndividualRegion.addStyleName("valTfIndividualRegion");
		valTfIndividualRegion.setVisible(false);
		culIndividual.addComponent(valTfIndividualRegion, "valTfIndividualRegion");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualRegion, valTfIndividualRegion);
		tfIndividualRegion.addListener(fieldRequiredBlurListener);
		*/
		culIndividual.addComponent(tfIndividualRegion, "tfIndividualRegion");
		
		tfIndividualCity = new TextField();
		tfIndividualCity.setValue("Омск");
		tfIndividualCity.addStyleName("tfIndividualCity");
		valTfIndividualCity = new Label("",Label.CONTENT_XHTML);
		valTfIndividualCity.addStyleName("valTfIndividualCity");
		valTfIndividualCity.addStyleName("validator");
		valTfIndividualCity.setVisible(false);
		culIndividual.addComponent(valTfIndividualCity, "valTfIndividualCity");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualCity, valTfIndividualCity);
		tfIndividualCity.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualCity, "tfIndividualCity");
		
		tfIndividualStreet = new TextField();
		tfIndividualStreet.addStyleName("tfIndividualStreet");
		valTfIndividualStreet = new Label("",Label.CONTENT_XHTML);
		valTfIndividualStreet.addStyleName("valTfIndividualStreet");
		valTfIndividualStreet.addStyleName("validator");
		valTfIndividualStreet.setVisible(false);
		culIndividual.addComponent(valTfIndividualStreet, "valTfIndividualStreet");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualStreet, valTfIndividualStreet);
		tfIndividualStreet.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualStreet, "tfIndividualStreet");
		
		tfIndividualHome = new TextField();
		tfIndividualHome.addStyleName("tfIndividualHome");
		valTfIndividualHome = new Label("",Label.CONTENT_XHTML);
		valTfIndividualHome.addStyleName("valTfIndividualHome");
		valTfIndividualHome.addStyleName("validator");
		valTfIndividualHome.setVisible(false);
		culIndividual.addComponent(valTfIndividualHome, "valTfIndividualHome");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualHome, valTfIndividualHome);
		tfIndividualHome.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualHome, "tfIndividualHome");
		
		tfIndividualHousing = new TextField();
		tfIndividualHousing.addStyleName("tfIndividualHousing");
		/*
		valTfIndividualHousing = new Label("",Label.CONTENT_XHTML);
		valTfIndividualHousing.addStyleName("valTfIndividualHousing");
		valTfIndividualHousing.setVisible(false);
		culIndividual.addComponent(valTfIndividualHousing, "valTfIndividualHousing");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualHousing, valTfIndividualHousing);
		tfIndividualHousing.addListener(fieldRequiredBlurListener);
		*/
		culIndividual.addComponent(tfIndividualHousing, "tfIndividualHousing");
		
		tfIndividualApartment = new TextField();
		tfIndividualApartment.addStyleName("tfIndividualApartment");
		/*
		valTfIndividualApartment = new Label("",Label.CONTENT_XHTML);
		valTfIndividualApartment.addStyleName("valTfIndividualApartment");
		valTfIndividualApartment.setVisible(false);
		culIndividual.addComponent(valTfIndividualApartment, "valTfIndividualApartment");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualApartment, valTfIndividualApartment);
		tfIndividualApartment.addListener(fieldRequiredBlurListener);
		*/
		culIndividual.addComponent(tfIndividualApartment, "tfIndividualApartment");
		
		tfIndividualNumberTelephone = new TextField();
		tfIndividualNumberTelephone.addStyleName("tfIndividualNumberTelephone");
		valTfIndividualNumberTelephone = new Label("",Label.CONTENT_XHTML);
		valTfIndividualNumberTelephone.addStyleName("valTfIndividualNumberTelephone");
		valTfIndividualNumberTelephone.addStyleName("validator");
		valTfIndividualNumberTelephone.setVisible(false);
		culIndividual.addComponent(valTfIndividualNumberTelephone, "valTfIndividualNumberTelephone");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualNumberTelephone, valTfIndividualNumberTelephone);
		tfIndividualNumberTelephone.addListener(fieldRequiredBlurListener);
		culIndividual.addComponent(tfIndividualNumberTelephone, "tfIndividualNumberTelephone");
		
		tfIndividualEmail = new TextField();
		tfIndividualEmail.addStyleName("tfIndividualEmail");
		valTfIndividualEmail = new Label("",Label.CONTENT_XHTML);
		valTfIndividualEmail.addStyleName("valTfIndividualEmail");
		valTfIndividualEmail.addStyleName("validator");
		valTfIndividualEmail.setVisible(false);
		culIndividual.addComponent(valTfIndividualEmail, "valTfIndividualEmail");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualEmail, valTfIndividualEmail);
		tfIndividualEmail.addListener(fieldRequiredBlurListener);
		emailValidateBlurListener = new EmailValidateBlurListener(tfIndividualEmail, valTfIndividualEmail);
		tfIndividualEmail.addListener(emailValidateBlurListener);
		culIndividual.addComponent(tfIndividualEmail, "tfIndividualEmail");
		
		return culIndividual;
	}
	
	private CustomLayout buildLegalLayout() {
		culLegal = new CustomLayout("legal");
		culLegal.addStyleName("culLegal");
		
		tfLegalFullName = new TextField();
		tfLegalFullName.addStyleName("tfLegalFullName");
		valTfLegalFullName = new Label("",Label.CONTENT_XHTML);
		valTfLegalFullName.addStyleName("valTfLegalFullName");
		valTfLegalFullName.addStyleName("validator");
		valTfLegalFullName.setVisible(true);
		culLegal.addComponent(valTfLegalFullName, "valTfLegalFullName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalFullName, valTfLegalFullName);
		tfLegalFullName.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalFullName, "tfLegalFullName");
		
		tfLegalBIN = new TextField();
		tfLegalBIN.addStyleName("tfLegalBIN");
		valTfLegalBIN = new Label("",Label.CONTENT_XHTML);
		valTfLegalBIN.addStyleName("valTfLegalBIN");
		valTfLegalBIN.addStyleName("validator");
		valTfLegalBIN.setVisible(true);
		culLegal.addComponent(valTfLegalBIN, "valTfLegalBIN");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalBIN, valTfLegalBIN);
		tfLegalBIN.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalBIN, "tfLegalBIN");
		
		tfLegalINN = new TextField();
		tfLegalINN.addStyleName("tfLegalINN");
		valTfLegalINN = new Label("",Label.CONTENT_XHTML);
		valTfLegalINN.addStyleName("valTfLegalINN");
		valTfLegalINN.addStyleName("validator");
		valTfLegalINN.setVisible(true);
		culLegal.addComponent(valTfLegalINN, "valTfLegalINN");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalINN, valTfLegalINN);
		tfLegalINN.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalINN, "tfLegalINN");
		
		tfLegalLastName = new TextField();
		tfLegalLastName.addStyleName("tfLegalLastName");
		valTfLegalLastName = new Label("",Label.CONTENT_XHTML);
		valTfLegalLastName.addStyleName("valTfLegalLastName");
		valTfLegalLastName.addStyleName("validator");
		valTfLegalLastName.setVisible(true);
		culLegal.addComponent(valTfLegalLastName, "valTfLegalLastName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalLastName, valTfLegalLastName);
		tfLegalLastName.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalLastName, "tfLegalLastName");
		
		tfLegalFirstName = new TextField();
		tfLegalFirstName.addStyleName("tfLegalFirstName");
		valTfLegalFirstName = new Label("",Label.CONTENT_XHTML);
		valTfLegalFirstName.addStyleName("valTfLegalFirstName");
		valTfLegalFirstName.addStyleName("validator");
		valTfLegalFirstName.setVisible(true);
		culLegal.addComponent(valTfLegalFirstName, "valTfLegalFirstName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalFirstName, valTfLegalFirstName);
		tfLegalFirstName.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalFirstName, "tfLegalFirstName");
		
		tfLegalMiddleName = new TextField();
		tfLegalMiddleName.addStyleName("tfLegalMiddleName");
		/*
		valTfLegalMiddleName = new Label("",Label.CONTENT_XHTML);
		valTfLegalMiddleName.addStyleName("valTfLegalMiddleName");
		valTfLegalMiddleName.addStyleName("validator");
		valTfLegalMiddleName.setVisible(true);
		culLegal.addComponent(valTfLegalMiddleName, "valTfLegalMiddleName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalMiddleName, valTfLegalMiddleName);
		tfLegalMiddleName.addListener(fieldRequiredBlurListener);
		*/
		culLegal.addComponent(tfLegalMiddleName, "tfLegalMiddleName");
		
		tfLegalPostHead = new TextField();
		tfLegalPostHead.addStyleName("tfLegalPostHead");
		valTfLegalPostHead = new Label("",Label.CONTENT_XHTML);
		valTfLegalPostHead.addStyleName("valTfLegalPostHead");
		valTfLegalPostHead.addStyleName("validator");
		valTfLegalPostHead.setVisible(true);
		culLegal.addComponent(valTfLegalPostHead, "valTfLegalPostHead");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalPostHead, valTfLegalPostHead);
		tfLegalPostHead.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalPostHead, "tfLegalPostHead");
		
		tfLegalCountry = new TextField();
		tfLegalCountry.setValue("Россия");
		tfLegalCountry.addStyleName("tfLegalCountry");
		valTfLegalCountry = new Label("",Label.CONTENT_XHTML);
		valTfLegalCountry.addStyleName("valTfLegalCountry");
		valTfLegalCountry.addStyleName("validator");
		valTfLegalCountry.setVisible(true);
		culLegal.addComponent(valTfLegalCountry, "valTfLegalCountry");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalCountry, valTfLegalCountry);
		tfLegalCountry.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalCountry, "tfLegalCountry");
		
		tfLegalRegion = new TextField();
		tfLegalRegion.setValue("Омская область");
		tfLegalRegion.addStyleName("tfLegalRegion");
		/*valTfLegalRegion = new Label("",Label.CONTENT_XHTML);
		valTfLegalRegion.addStyleName("valTfLegalRegion");
		valTfLegalRegion.setVisible(true);
		culLegal.addComponent(valTfLegalRegion, "valTfLegalRegion");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalRegion, valTfLegalRegion);
		tfLegalRegion.addListener(fieldRequiredBlurListener);
		*/
		culLegal.addComponent(tfLegalRegion, "tfLegalRegion");
		
		tfLegalCity = new TextField();
		tfLegalCity.setValue("Омск");
		tfLegalCity.addStyleName("tfLegalCity");
		valTfLegalCity = new Label("",Label.CONTENT_XHTML);
		valTfLegalCity.addStyleName("valTfLegalCity");
		valTfLegalCity.addStyleName("validator");
		valTfLegalCity.setVisible(true);
		culLegal.addComponent(valTfLegalCity, "valTfLegalCity");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalCity, valTfLegalCity);
		tfLegalCity.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalCity, "tfLegalCity");
		
		tfLegalStreet = new TextField();
		tfLegalStreet.addStyleName("tfLegalStreet");
		valTfLegalStreet = new Label("",Label.CONTENT_XHTML);
		valTfLegalStreet.addStyleName("valTfLegalStreet");
		valTfLegalStreet.addStyleName("validator");
		valTfLegalStreet.setVisible(true);
		culLegal.addComponent(valTfLegalStreet, "valTfLegalStreet");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalStreet, valTfLegalStreet);
		tfLegalStreet.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalStreet, "tfLegalStreet");
		
		tfLegalHome = new TextField();
		tfLegalHome.addStyleName("tfLegalHome");
		valTfLegalHome = new Label("",Label.CONTENT_XHTML);
		valTfLegalHome.addStyleName("valTfLegalHome");
		valTfLegalHome.addStyleName("validator");
		valTfLegalHome.setVisible(true);
		culLegal.addComponent(valTfLegalHome, "valTfLegalHome");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalHome, valTfLegalHome);
		tfLegalHome.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalHome, "tfLegalHome");
		
		tfLegalHousing = new TextField();
		tfLegalHousing.addStyleName("tfLegalHousing");
		/*
		valTfLegalHousing = new Label("",Label.CONTENT_XHTML);
		valTfLegalHousing.addStyleName("valTfLegalHousing");
		valTfLegalHousing.setVisible(true);
		culLegal.addComponent(valTfLegalHousing, "valTfLegalHousing");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalHousing, valTfLegalHousing);
		tfLegalHousing.addListener(fieldRequiredBlurListener);
		*/
		culLegal.addComponent(tfLegalHousing, "tfLegalHousing");
		
		tfLegalApartment = new TextField();
		tfLegalApartment.addStyleName("tfLegalApartment");
		/*
		valTfLegalApartment = new Label("",Label.CONTENT_XHTML);
		valTfLegalApartment.addStyleName("valTfLegalApartment");
		valTfLegalApartment.setVisible(true);
		culLegal.addComponent(valTfLegalApartment, "valTfLegalApartment");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalApartment, valTfLegalApartment);
		tfLegalApartment.addListener(fieldRequiredBlurListener);
		*/
		culLegal.addComponent(tfLegalApartment, "tfLegalApartment");
		
		culLegalIndividual = new CustomLayout("legal_individual");
		culLegalIndividual.addStyleName("culLegalIndividual");
		culLegalIndividual.setVisible(false);
		viewLayout.addComponent(culLegalIndividual, "culLegalIndividual");
		
		tfLegalIndividualLastName = new TextField();
		tfLegalIndividualLastName.addStyleName("tfIndividualLastName");
		valTfLegalIndividualLastName = new Label("",Label.CONTENT_XHTML);
		valTfLegalIndividualLastName.addStyleName("valTfIndividualLastName");
		valTfLegalIndividualLastName.addStyleName("validator");
		valTfLegalIndividualLastName.setVisible(true);
		culLegalIndividual.addComponent(valTfLegalIndividualLastName, "valTfLegalIndividualLastName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalIndividualLastName, valTfLegalIndividualLastName);
		tfLegalIndividualLastName.addListener(fieldRequiredBlurListener);
		culLegalIndividual.addComponent(tfLegalIndividualLastName, "tfLegalIndividualLastName");
		
		tfLegalIndividualFirstName = new TextField();
		tfLegalIndividualFirstName.addStyleName("tfIndividualFirstName");
		valTfLegalIndividualFirstName = new Label("",Label.CONTENT_XHTML);
		valTfLegalIndividualFirstName.addStyleName("valTfIndividualFirstName");
		valTfLegalIndividualFirstName.addStyleName("validator");
		valTfLegalIndividualFirstName.setVisible(false);
		culLegalIndividual.addComponent(valTfLegalIndividualFirstName, "valTfLegalIndividualFirstName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalIndividualFirstName, valTfLegalIndividualFirstName);
		tfLegalIndividualFirstName.addListener(fieldRequiredBlurListener);
		culLegalIndividual.addComponent(tfLegalIndividualFirstName, "tfLegalIndividualFirstName");
		
		tfLegalIndividualMiddleName = new TextField();
		tfLegalIndividualMiddleName.addStyleName("tfIndividualMiddleName");
		/*valTfIndividualMiddleName = new Label("",Label.CONTENT_XHTML);
		valTfIndividualMiddleName.addStyleName("valTfIndividualMiddleName");
		valTfIndividualMiddleName.addStyleName("validator");
		valTfIndividualMiddleName.setVisible(false);
		culIndividual.addComponent(valTfIndividualMiddleName, "valTfIndividualMiddleName");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfIndividualMiddleName, valTfIndividualMiddleName);
		tfIndividualMiddleName.addListener(fieldRequiredBlurListener);
		*/
		culLegalIndividual.addComponent(tfLegalIndividualMiddleName, "tfLegalIndividualMiddleName");
		
		//Бакыт 27 ноября 2015 попросила, чтобы смена фамилии горела всегда
		/*
		cbLegalIndividualChangeLastName = new CheckBox();
		cbLegalIndividualChangeLastName.addStyleName("cbIndividualChangeLastName");
		cbLegalIndividualChangeLastName.setImmediate(true);
		cbLegalIndividualChangeLastName.addListener(new ValueChangeListener() {
			
			public void valueChange(ValueChangeEvent event) {
				culLegalIndividualChangeLastName.setVisible(cbLegalIndividualChangeLastName.booleanValue());
			}
		});
		culLegalIndividual.addComponent(cbLegalIndividualChangeLastName, "cbLegalIndividualChangeLastName");
		*/
		
		culLegalIndividualChangeLastName = new CustomLayout("legal_individual_change_lastname");
		culLegalIndividualChangeLastName.setVisible(true);
		tfLegalIndividualChangeLastName = new TextField();
		tfLegalIndividualChangeLastName.addStyleName("tfIndividualChangeLastName");
		//Установить ширину
		tfLegalIndividualChangeLastName.setWidth("57%");
		culLegalIndividualChangeLastName.addComponent(tfLegalIndividualChangeLastName, "tfLegalIndividualChangeLastName");
		culLegalIndividual.addComponent(culLegalIndividualChangeLastName, "culLegalIndividualChangeLastName");
		
		culLegalIndividualDatesBirth = new CustomLayout("legal-individual-dates-birth");
		culLegalIndividualDatesBirth.addStyleName("culLegalIndividualDatesBirth");
		culLegalIndividualDatesBirth.setImmediate(true);
		//Бакыт 27 ноября попросила, чтобы даты детей горели всегда
		//culLegalIndividualDatesBirth.setVisible(false);
		culLegalIndividualDatesBirth.setVisible(true);
		
		cssLegalIndividualDatesBirth = new CssLayout();
		
		valCssLegalIndividualDatesBirth = new Label("",Label.CONTENT_XHTML);
		valCssLegalIndividualDatesBirth.addStyleName("valCssIndividualDatesBirth");
		valCssLegalIndividualDatesBirth.addStyleName("validator");
		valCssLegalIndividualDatesBirth.setVisible(false);
		culLegalIndividualDatesBirth.addComponent(valCssLegalIndividualDatesBirth, "valCssLegalIndividualDatesBirth");
		
		bAddLegalIndividualDatesBirth = new Button("Добавить дату");
		bAddLegalIndividualDatesBirth.addStyleName("bAddDatesBirth");
		
		//добавить 1 поле даты по умолчанию
		PopupDateField pdfDatesBirth = new PopupDateField();
		pdfDatesBirth.setValidationVisible(false);
		pdfDatesBirth.addStyleName("pdfDatesBirth" + cssLegalIndividualDatesBirth.getComponentCount());
		pdfDatesBirth.setResolution(4);
		pdfDatesBirth.setImmediate(true);
		cssLegalIndividualDatesBirth.addComponent(pdfDatesBirth);
		
		bAddLegalIndividualDatesBirth.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				PopupDateField pdfDatesBirth = new PopupDateField();
				pdfDatesBirth.setValidationVisible(false);
				pdfDatesBirth.addStyleName("pdfDatesBirth" + cssLegalIndividualDatesBirth.getComponentCount());
				pdfDatesBirth.setResolution(4);
				pdfDatesBirth.setImmediate(true);
				//pdfDatesBirth.addListener(bLDatesBirth);
				cssLegalIndividualDatesBirth.addComponent(pdfDatesBirth);
				//Установим последний компонент, для быстрого доступа
				if (cssLegalIndividualDatesBirth.getComponentCount()>1) pdfDatesBirth.setData(cssLegalIndividualDatesBirth.getData());
				cssLegalIndividualDatesBirth.setData(pdfDatesBirth);
				//если компонент дат > 1 то включить кнопку удалить дату
				if (cssLegalIndividualDatesBirth.getComponentCount()>1) bDeleteLegalIndividualDatesBirth.setVisible(true); 
			}
		});
		culLegalIndividualDatesBirth.addComponent(bAddLegalIndividualDatesBirth, "bAddLegalIndividualDatesBirth");
		
		bDeleteLegalIndividualDatesBirth = new Button("Удалить дату");
		bDeleteLegalIndividualDatesBirth.setVisible(false);
		bDeleteLegalIndividualDatesBirth.addStyleName("bDeleteDatesBirth");
		bDeleteLegalIndividualDatesBirth.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				Component removeComponent = (Component)cssLegalIndividualDatesBirth.getData();
				//removeComponent.removeStyleName("");
				if (removeComponent != null) {
					cssLegalIndividualDatesBirth.setData(((PopupDateField)removeComponent).getData());
					cssLegalIndividualDatesBirth.removeComponent(removeComponent);
				}
				//если компонент дат = 1 то включить кнопку удалить дату
				if (cssLegalIndividualDatesBirth.getComponentCount() == 1) bDeleteLegalIndividualDatesBirth.setVisible(false);
			}
		});
		culLegalIndividualDatesBirth.addComponent(bDeleteLegalIndividualDatesBirth, "bDeleteLegalIndividualDatesBirth");
		culLegalIndividualDatesBirth.addComponent(cssLegalIndividualDatesBirth, "cssLegalIndividualDatesBirth");
		culLegalIndividual.addComponent(culLegalIndividualDatesBirth, "culLegalIndividualDatesBirth");
		
		culDatesBirth = new CustomLayout("dates-birth");
		culDatesBirth.addStyleName("culDatesBirth");
		//Бакыт 27 ноября попросила, чтобы даты детей горели всегда
		//culDatesBirth.setVisible(false);
		culDatesBirth.setVisible(true);
		cssDatesBirth = new CssLayout();
		
		valCssDatesBirth = new Label("",Label.CONTENT_XHTML);
		valCssDatesBirth.addStyleName("valCssDatesBirth");
		valCssDatesBirth.addStyleName("validator");
		valCssDatesBirth.setVisible(false);
		culDatesBirth.addComponent(valCssDatesBirth, "valCssDatesBirth");
		//Слушатель, для проверки введенной даты
		/*final BlurListener bLDatesBirth = new BlurListener() {
			
			public void blur(BlurEvent event) {
				PopupDateField pdf = (PopupDateField)event.getComponent();
				System.out.println("event.getSource().toString()="+event.);
				System.out.println("Положение 1");
				if (pdf.getValue() == null) {
					System.out.println("Положение 2");
					valCssDatesBirth.setVisible(false);
					valCssDatesBirth.setValue(null);
					return;
				}
				if (pdf.getValue() instanceof Date) {
					valCssDatesBirth.setVisible(false);
					valCssDatesBirth.setValue(null);
					return;
				} else {
					valCssDatesBirth.setVisible(true);
					valCssDatesBirth.setValue("Присутствуют некорректно введенные даты.");
					return;
				}
			}
		};*/
		
		bAddDatesBirth = new Button("Добавить дату");
		bAddDatesBirth.addStyleName("bAddDatesBirth");
		
		//добавить 1 поле даты по умолчанию
		pdfDatesBirth = new PopupDateField();
		pdfDatesBirth.setValidationVisible(false);
		pdfDatesBirth.addStyleName("pdfDatesBirth" + cssDatesBirth.getComponentCount());
		pdfDatesBirth.setResolution(4);
		pdfDatesBirth.setImmediate(true);
		cssDatesBirth.addComponent(pdfDatesBirth);
		
		bAddDatesBirth.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				PopupDateField pdfDatesBirth = new PopupDateField();
				pdfDatesBirth.setValidationVisible(false);
				pdfDatesBirth.addStyleName("pdfDatesBirth" + cssDatesBirth.getComponentCount());
				pdfDatesBirth.setResolution(4);
				pdfDatesBirth.setImmediate(true);
				//pdfDatesBirth.addListener(bLDatesBirth);
				cssDatesBirth.addComponent(pdfDatesBirth);
				//Установим последний компонент, для быстрого доступа
				if (cssDatesBirth.getComponentCount()>1) pdfDatesBirth.setData(cssDatesBirth.getData());
				cssDatesBirth.setData(pdfDatesBirth);
				//если компонент дат > 1 то включить кнопку удалить дату
				if (cssDatesBirth.getComponentCount()>1) bDeleteDatesBirth.setVisible(true); 
			}
		});
		culDatesBirth.addComponent(bAddDatesBirth, "bAddDatesBirth");
		
		bDeleteDatesBirth = new Button("Удалить дату");
		bDeleteDatesBirth.setVisible(false);
		bDeleteDatesBirth.addStyleName("bDeleteDatesBirth");
		bDeleteDatesBirth.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				Component removeComponent = (Component)cssDatesBirth.getData();
				//removeComponent.removeStyleName("");
				if (removeComponent != null) {
					cssDatesBirth.setData(((PopupDateField)removeComponent).getData());
					cssDatesBirth.removeComponent(removeComponent);
				}
				//если компонент дат = 1 то включить кнопку удалить дату
				if (cssDatesBirth.getComponentCount() == 1) bDeleteDatesBirth.setVisible(false);
			}
		});
		culDatesBirth.addComponent(bDeleteDatesBirth, "bDeleteDatesBirth");
		culDatesBirth.addComponent(cssDatesBirth, "cssDatesBirth");
		culIndividual.addComponent(culDatesBirth, "culDatesBirth");
		
		/*
		tfLegalMailAddress = new TextField();
		tfLegalMailAddress.setWordwrap(true);
		tfLegalMailAddress.setRows(2);
		tfLegalMailAddress.addStyleName("tfLegalMailAddress");
		valTfLegalMailAddress = new Label("",Label.CONTENT_XHTML);
		valTfLegalMailAddress.addStyleName("valTfLegalMailAddress");
		valTfLegalMailAddress.setVisible(true);
		culLegal.addComponent(valTfLegalMailAddress, "valTfLegalMailAddress");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalMailAddress, valTfLegalMailAddress);
		tfLegalMailAddress.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalMailAddress, "tfLegalMailAddress");
		*/
		
		tfLegalNumberTelephone = new TextField();
		tfLegalNumberTelephone.addStyleName("tfLegalNumberTelephone");
		valTfLegalNumberTelephone = new Label("",Label.CONTENT_XHTML);
		valTfLegalNumberTelephone.addStyleName("valTfLegalNumberTelephone");
		valTfLegalNumberTelephone.addStyleName("validator");
		valTfLegalNumberTelephone.setVisible(true);
		culLegal.addComponent(valTfLegalNumberTelephone, "valTfLegalNumberTelephone");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalNumberTelephone, valTfLegalNumberTelephone);
		tfLegalNumberTelephone.addListener(fieldRequiredBlurListener);
		culLegal.addComponent(tfLegalNumberTelephone, "tfLegalNumberTelephone");
		
		tfLegalEmail = new TextField();
		tfLegalEmail.addStyleName("tfLegalEmail");
		valTfLegalEmail = new Label("",Label.CONTENT_XHTML);
		valTfLegalEmail.addStyleName("valTfLegalEmail");
		valTfLegalEmail.addStyleName("validator");
		valTfLegalEmail.setVisible(true);
		culLegal.addComponent(valTfLegalEmail, "valTfLegalEmail");
		fieldRequiredBlurListener = new FieldRequiredBlurListener(tfLegalEmail, valTfLegalEmail);
		tfLegalEmail.addListener(fieldRequiredBlurListener);
		emailValidateBlurListener = new EmailValidateBlurListener(tfLegalEmail, valTfLegalEmail);
		tfLegalEmail.addListener(emailValidateBlurListener);
		culLegal.addComponent(tfLegalEmail, "tfLegalEmail");
		
		return culLegal;
	}
	
	private CustomLayout buildErrorLayout() {
		culError = new CustomLayout("error");
		culError.addStyleName("culError");
		return culError;
	}
	
	private CustomLayout buildSentLayout() {
		culSent = new CustomLayout("sent");
		culSent.addStyleName("culSent");
		return culSent;
	}
	
	private void buildUploaderCopyWorkRecord() {
		
		uCopyWorkRecord = new Upload(null, myReceiver);
		uCopyWorkRecord.addStyleName("uCopyWorkRecord");
		uCopyWorkRecord.setImmediate(true);
		
		uCopyWorkRecord.setButtonCaption("Добавить файл");
		
		//getWindow().addComponent(uploadRefresher.create());
		
		//final Double maxSizeFile=999995242880.0;
		final Double maxSizeFile = 5242880.0;//5мб
		final int maxFiles = 5;
		//final Integer[] countUpload = {0};
		final ArrayList<File> uploadFiles = new ArrayList<File>();
		
		//bCancelUpload = new Button("Отмена");
		bCancelUpload = new Button("Удалить последний файл");
		bCancelUpload.addStyleName("bCancelUpload");
		bCancelUpload.setImmediate(true);
		bCancelUpload.setVisible(false);
		//кнопка имеет 2 режима, загрузка файла и удаление последнего загруженного файла
		bCancelUpload.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				//Если загрузка файла, то кнопка отменяет загрузку
				if (!uCopyWorkRecord.isUploading()) {//Если нет загрузки файла, то кнопка удаляет последний загруженный файл
					
					if ( ((MyReceiver)uCopyWorkRecord.getReceiver()).getDirOfReceiveFile() != null) {
						
						//((MyReceiver)uCopyWorkRecord.getReceiver()).deleteLastReceiveFile();
						//удалить файл из ArrayList и из папки
						int lastIndex = uploadFiles.size() - 1;
						if (lastIndex >= 0) {
							myReceiver.deleteAnyFile(uploadFiles.get(lastIndex));
							uploadFiles.remove(lastIndex);
						}
						
						uCopyWorkRecord.setEnabled(true);
						valUCopyWorkRecord.setVisible(false);
						valUCopyWorkRecord.setValue(null);
						
						//int countFiles = maxFiles - ((MyReceiver)uCopyWorkRecord.getReceiver()).getCountFilesDirOfReceiveFile();
						int countFiles = maxFiles - uploadFiles.size();
				    	String str = "файлов";
				    	if (countFiles == 0) {
				    		valCounterFiles.setValue("Вы не можете больше добавлять файлы");
				    		uCopyWorkRecord.setEnabled(false);
				    	}
				    	if (countFiles == 1) str = "файл";
				    	if (countFiles >= 2 && countFiles <= 4) str = "файла";
				    	if (countFiles >= 5) str = "файлов";
				    	
				    	if (countFiles >= 1 && countFiles <= maxFiles) {
				    		valCounterFiles.setValue(new StringBuffer("Вы можете добавить еще ").append(countFiles).append(" ").append(str));
				    		uCopyWorkRecord.setEnabled(true);
				    	}
						
						String delStr = filesUCopyWorkRecord.getValue().toString();
						int bDel = filesUCopyWorkRecord.getValue().toString().lastIndexOf("<div>");
						int eDel = filesUCopyWorkRecord.getValue().toString().length();
						
						StringBuffer sbDelStr = new StringBuffer(delStr);
						sbDelStr.delete(bDel, eDel);
						
						filesUCopyWorkRecord.setValue(sbDelStr.toString());
						
						if (filesUCopyWorkRecord.getValue() == null || filesUCopyWorkRecord.getValue().equals("")) {
							filesUCopyWorkRecord.setVisible(false);
							bCancelUpload.setVisible(false);
						}
						
					}
				}
			}
			
		});
		culStep2Work.addComponent(bCancelUpload, "bCancelUpload");
		
		//Button bDeleteUpload = new Button("Отмена");
		//culStep2Work.addComponent(bDeleteUpload, "bDeleteUpload");
		
		uCopyWorkRecord.addListener(new Upload.StartedListener() {
			
	        public void uploadStarted(StartedEvent event) {
	        	
	        	//uploadRefresher.setRefreshInterval(1000);
	        	//uploadRefresher.startRefresh();
				
	        	/*
				valUCopyWorkRecord.removeStyleName("validator");
		        valUCopyWorkRecord.removeStyleName("okStatus");
		        valUCopyWorkRecord.addStyleName("loadStatus");
		        valUCopyWorkRecord.setVisible(true);
	            valUCopyWorkRecord.setValue("<div>Загрузка файла \"" + event.getFilename() + "\"</div>");
	            */
	        	
	        	uCopyWorkRecord.setImmediate(true);
	        	bSend.setEnabled(false);
	        	
	        	valUCopyWorkRecord.setVisible(true);
	        	uCopyWorkRecord.setEnabled(false);
	        	
	        	/*
	        	bCancelUpload.setImmediate(true);
	        	bCancelUpload.setCaption("Отмена");
	            bCancelUpload.setVisible(true);
	            */
	        	
	        	if ((((MyReceiver)uCopyWorkRecord.getReceiver()).getDirOfReceiveFile() != null)) {
	        		
		        	//if ((((MyReceiver)uCopyWorkRecord.getReceiver()).getCountFilesDirOfReceiveFile()) == maxFiles) {
	        		if (uploadFiles.size() == maxFiles) {
						
				    	valUCopyWorkRecord.removeStyleName("loadStatus");
				        valUCopyWorkRecord.removeStyleName("okStatus");
				        valUCopyWorkRecord.addStyleName("validator");
		        		valUCopyWorkRecord.setData("Невозможно загрузить более " + String.valueOf(maxFiles) +  " файлов");
		        		uCopyWorkRecord.setEnabled(false);
		        		uCopyWorkRecord.interruptUpload();
		        		return;
					}
	        	}
	        	
	        	if ("zip, pdf, doc (docx), jpg (jpeg), png, gif, bmp, tif (tiff)".indexOf(MyFileUtils.getExtNotDot(event.getFilename())) == -1) {
					//прервать загрузку
	        		valUCopyWorkRecord.removeStyleName("loadStatus");
			        valUCopyWorkRecord.removeStyleName("okStatus");
			        valUCopyWorkRecord.addStyleName("validator");
	        		valUCopyWorkRecord.setData("Данный формат файла не поддерживается");
	        		uCopyWorkRecord.setEnabled(true);
	        		uCopyWorkRecord.interruptUpload();
	        		return;
				}
	        	
	        	if (Double.valueOf(String.valueOf(event.getContentLength()))>maxSizeFile) {
					//прервать загрузку
	        		valUCopyWorkRecord.removeStyleName("loadStatus");
			        valUCopyWorkRecord.removeStyleName("okStatus");
			        valUCopyWorkRecord.addStyleName("validator");
	        		valUCopyWorkRecord.setData("Файл превышает допустимый размер 5 мб");
	        		uCopyWorkRecord.setEnabled(true);
	        		uCopyWorkRecord.interruptUpload();
	        		return;
				}
	        	
	        }
	
		});
		
		uCopyWorkRecord.addListener(new Upload.ProgressListener() {
		    public void updateProgress(long readBytes, long contentLength) {
		    	
		    	//System.out.println("Upload.ProgressListener");
		    	/*uCopyWorkRecord.setImmediate(true);
		        valUCopyWorkRecord.removeStyleName("validator");
		        valUCopyWorkRecord.removeStyleName("okStatus");
		        valUCopyWorkRecord.addStyleName("loadStatus");
		        //valUCopyWorkRecord.setValue("Загрузка файла \"" + receiver.getFileName() + "\"");
		        valUCopyWorkRecord.setVisible(true);
		        valUCopyWorkRecord.setValue("<div>Загрузка файла</div>");*/
		    }
		
		});
		
		uCopyWorkRecord.addListener(new Upload.SucceededListener() {
		    public void uploadSucceeded(SucceededEvent event) {
		    	uploadFiles.add(myReceiver.getReceiveFile());
		    	uCopyWorkRecord.setImmediate(true);
		    	uCopyWorkRecord.setEnabled(true);
		    	uCopyWorkRecord.requestRepaint();
		    	bSend.setEnabled(true);
		    	
		    	//int countFiles = maxFiles - ((MyReceiver)uCopyWorkRecord.getReceiver()).getCountFilesDirOfReceiveFile();
		    	int countFiles = maxFiles - uploadFiles.size();
		    	String str = "файлов";
		    	if (countFiles == 0) {
		    		valCounterFiles.setValue("Вы не можете больше добавлять файлы");
		    		uCopyWorkRecord.setEnabled(false);
		    	}
		    	if (countFiles == 1) str = "файл";
		    	if (countFiles >= 2 && countFiles <= 4) str = "файла";
		    	if (countFiles >= 5) str = "файлов";
		    	
		    	if (countFiles >= 1 && countFiles <= maxFiles) valCounterFiles.setValue(new StringBuffer("Вы можете добавить еще ").append(countFiles).append(" ").append(str));
		    	
		        // This method gets called when the uCopyWorkRecord finished successfully
		    	valUCopyWorkRecord.removeStyleName("validator");
		        valUCopyWorkRecord.removeStyleName("loadStatus");
		        valUCopyWorkRecord.addStyleName("okStatus");
		        valUCopyWorkRecord.setVisible(true);
		        valUCopyWorkRecord.setValue("");
		        
		    	filesUCopyWorkRecord.addStyleName("okStatus");
		        filesUCopyWorkRecord.setVisible(true);
		        if (filesUCopyWorkRecord.getValue() == null) filesUCopyWorkRecord.setValue("");
		        else filesUCopyWorkRecord.setValue(filesUCopyWorkRecord.getValue() + "<div>Загружен файл " + event.getFilename() + "</div>");
		        
		        bCancelUpload.setVisible(true);
		    	//bCancelUpload.setCaption("Удалить последний файл");
		    	//myRefresher.stopRefresh();
		    	//uploadRefresher.stopRefresh();
				
		    }
		});
		
		uCopyWorkRecord.addListener(new Upload.FailedListener() {
		    public void uploadFailed(FailedEvent event) {
		    	uCopyWorkRecord.setImmediate(true);
		    	bSend.setEnabled(true);
		        // This method gets called when the uCopyWorkRecord failed
		    	valUCopyWorkRecord.removeStyleName("okStatus");
		        valUCopyWorkRecord.removeStyleName("loadStatus");
		        valUCopyWorkRecord.addStyleName("validator");
		        valUCopyWorkRecord.setVisible(true);
		    	valUCopyWorkRecord.setValue("<div>" + valUCopyWorkRecord.getData() + "</div>");

		    	//удалить папку с файлами аплоада
		    	//((MyReceiver)uCopyWorkRecord.getReceiver()).deleteDirOfReceiveFile();
		    	((MyReceiver)uCopyWorkRecord.getReceiver()).deleteReceiveFile();
		    	
		    	//если есть файлы, то переименовать кнопку
		    	//if (((MyReceiver)uCopyWorkRecord.getReceiver()).getCountFilesDirOfReceiveFile() > 0) {
		    	if (uploadFiles.size() > 0) {
		    		//bCancelUpload.setCaption("Удалить последний файл");
		    		bCancelUpload.setVisible(true);
		    	} else {
		    		//bCancelUpload.setCaption("Отмена");
		    		bCancelUpload.setVisible(false);
		    	}
		    	
		    	//bCancelUpload.setVisible(false);
		    	//myRefresher.stopRefresh();
				//uCopyWorkRecord.setEnabled(true);
		    	//uploadRefresher.stopRefresh();
		    }
		});
		
		uCopyWorkRecord.addListener(new Upload.FinishedListener() {
		    public void uploadFinished(FinishedEvent event) {
		    	//System.out.println("Upload.FinishedListener");
		    	uCopyWorkRecord.setImmediate(true);
		        // This method gets called always when the uCopyWorkRecord finished,
				// either succeeding or failing
				//uCopyWorkRecord.setVisible(true);
				//uCopyWorkRecord.setCaption("Select another file");
		    	//bCancelUpload.setCaption("Удалить последний файл");
		    }
		
		});
		
		//Слушать закрытия окна, удалить файл
		addListener(new CloseListener() {
			
			public void windowClose(CloseEvent e) {
				//удалить папку с файлами аплоада
				((MyReceiver)uCopyWorkRecord.getReceiver()).deleteDirOfReceiveFile();
				valUCopyWorkRecord.setVisible(false);
				valUCopyWorkRecord.setValue(null);
			}
		});
		
		
    	String str = "файлов";
    	if (maxFiles == 1) str = "файл";
    	if (maxFiles >= 2 && maxFiles <= 4) str = "файла";
    	if (maxFiles >= 5) str = "файлов";
		
		valCounterFiles = new Label("Вы можете добавить еще " + String.valueOf(maxFiles) + " " + str ,Label.CONTENT_XHTML);
		valCounterFiles.addStyleName("valCounterFiles");
		valCounterFiles.addStyleName("validator");
		valCounterFiles.setVisible(true);
		
		valUCopyWorkRecord = new Label("",Label.CONTENT_XHTML);
		valUCopyWorkRecord.addStyleName("valUCopyWorkRecord");
		valUCopyWorkRecord.addStyleName("validator");
		valUCopyWorkRecord.setVisible(false);
		
		filesUCopyWorkRecord = new Label("",Label.CONTENT_XHTML);
		filesUCopyWorkRecord.addStyleName("valUCopyWorkRecord");
		filesUCopyWorkRecord.addStyleName("validator");
		filesUCopyWorkRecord.setVisible(false);
		
		//fieldRequiredBlurListener = new FieldRequiredBlurListener(uCopyWorkRecord, valUCopyWorkRecord);
		//uCopyWorkRecord.addListener(fieldRequiredBlurListener);
		culStep2Work.addComponent(valCounterFiles, "valCounterFiles");
		culStep2Work.addComponent(valUCopyWorkRecord, "valUCopyWorkRecord");
		culStep2Work.addComponent(filesUCopyWorkRecord, "filesUCopyWorkRecord");
		culStep2Work.addComponent(uCopyWorkRecord, "uCopyWorkRecord");
		
	}
	
	private void buildUploaderPassport() {
		
		uCopyPassport = new Upload(null, myReceiver);
		uCopyPassport.addStyleName("uCopyPassport");
		uCopyPassport.setImmediate(true);
		
		uCopyPassport.setButtonCaption("Добавить файл");
		
		//final Double maxSizeFile=999995242880.0;
		final Double maxSizeFile = 5242880.0;//5мб
		final int maxFiles = 5;
		//final Integer[] countUpload = {0};
		final ArrayList<File> uploadFiles = new ArrayList<File>();
		uCopyPassport.setData(uploadFiles);
		bCancelUploadPassport = new Button("Удалить последний файл");
		bCancelUploadPassport.addStyleName("bCancelUploadPassport");
		bCancelUploadPassport.setImmediate(true);
		bCancelUploadPassport.setVisible(false);
		//кнопка имеет 2 режима, загрузка файла и удаление последнего загруженного файла
		bCancelUploadPassport.addListener(new ClickListener() {
			
			public void buttonClick(ClickEvent event) {
				//Если загрузка файла, то кнопка отменяет загрузку
				if (!uCopyPassport.isUploading()) {//Если нет загрузки файла, то кнопка удаляет последний загруженный файл
					
					if ( ((MyReceiver)uCopyPassport.getReceiver()).getDirOfReceiveFile() != null) {
						
						//((MyReceiver)uCopyPassport.getReceiver()).deleteLastReceiveFile();
						//удалить файл из ArrayList и из папки
						int lastIndex = uploadFiles.size() - 1;
						if (lastIndex >= 0) {
							myReceiver.deleteAnyFile(uploadFiles.get(lastIndex));
							uploadFiles.remove(lastIndex);
						}
						
						uCopyPassport.setEnabled(true);
						valUCopyPassport.setVisible(false);
						valUCopyPassport.setValue(null);
						
						//int countFiles = maxFiles - ((MyReceiver)uCopyPassport.getReceiver()).getCountFilesDirOfReceiveFile();
						int countFiles = maxFiles - uploadFiles.size();
				    	String str = "файлов";
				    	if (countFiles == 0) {
				    		valCounterFilesPassport.setValue("Вы не можете больше добавлять файлы");
				    		uCopyPassport.setEnabled(false);
				    	}
				    	if (countFiles == 1) str = "файл";
				    	if (countFiles >= 2 && countFiles <= 4) str = "файла";
				    	if (countFiles >= 5) str = "файлов";
				    	
				    	if (countFiles >= 1 && countFiles <= maxFiles) {
				    		valCounterFilesPassport.setValue(new StringBuffer("Вы можете добавить еще ").append(countFiles).append(" ").append(str));
				    		uCopyPassport.setEnabled(true);
				    	}
						
						String delStr = filesUCopyPassport.getValue().toString();
						int bDel = filesUCopyPassport.getValue().toString().lastIndexOf("<div>");
						int eDel = filesUCopyPassport.getValue().toString().length();
						
						StringBuffer sbDelStr = new StringBuffer(delStr);
						sbDelStr.delete(bDel, eDel);
						
						filesUCopyPassport.setValue(sbDelStr.toString());
						
						if (filesUCopyPassport.getValue() == null || filesUCopyPassport.getValue().equals("")) {
							filesUCopyPassport.setVisible(false);
							bCancelUploadPassport.setVisible(false);
						}
						
					}
				}
			}
			
		});
		
		//Button bDeleteUploadPassport = new Button("Отмена");
		
		
		uCopyPassport.addListener(new Upload.StartedListener() {
			
	        public void uploadStarted(StartedEvent event) {
	        	
	        	uCopyPassport.setImmediate(true);
	        	bSend.setEnabled(false);
	        	
	        	valUCopyPassport.setVisible(true);
	        	uCopyPassport.setEnabled(false);
	        	
	        	/*
	        	bCancelUpload.setImmediate(true);
	        	bCancelUpload.setCaption("Отмена");
	            bCancelUpload.setVisible(true);
	            */
	        	
	        	if ((((MyReceiver)uCopyPassport.getReceiver()).getDirOfReceiveFile() != null)) {
	        		
		        	//if ((((MyReceiver)uCopyPassport.getReceiver()).getCountFilesDirOfReceiveFile()) == maxFiles) {
	        		if (uploadFiles.size() == maxFiles) {
						
				    	valUCopyPassport.removeStyleName("loadStatus");
				        valUCopyPassport.removeStyleName("okStatus");
				        valUCopyPassport.addStyleName("validator");
		        		valUCopyPassport.setData("Невозможно загрузить более " + String.valueOf(maxFiles) + " файлов");
		        		uCopyPassport.setEnabled(false);
		        		uCopyPassport.interruptUpload();
		        		return;
					}
	        	}
	        	
	        	if ("zip, pdf, doc (docx), jpg (jpeg), png, gif, bmp, tif (tiff)".indexOf(MyFileUtils.getExtNotDot(event.getFilename())) == -1) {
					//прервать загрузку
	        		valUCopyPassport.removeStyleName("loadStatus");
			        valUCopyPassport.removeStyleName("okStatus");
			        valUCopyPassport.addStyleName("validator");
	        		valUCopyPassport.setData("Данный формат файла не поддерживается");
	        		uCopyPassport.setEnabled(true);
	        		uCopyPassport.interruptUpload();
	        		return;
				}
	        	
	        	if (Double.valueOf(String.valueOf(event.getContentLength()))>maxSizeFile) {
					//прервать загрузку
	        		valUCopyPassport.removeStyleName("loadStatus");
			        valUCopyPassport.removeStyleName("okStatus");
			        valUCopyPassport.addStyleName("validator");
	        		valUCopyPassport.setData("Файл превышает допустимый размер 5 мб");
	        		uCopyPassport.setEnabled(true);
	        		uCopyPassport.interruptUpload();
	        		return;
				}
	        	
	        }
	
		});
		
		uCopyPassport.addListener(new Upload.ProgressListener() {
		    public void updateProgress(long readBytes, long contentLength) {
		    	
		    	//System.out.println("Upload.ProgressListener");
		    	/*uCopyPassport.setImmediate(true);
		        valUCopyPassport.removeStyleName("validator");
		        valUCopyPassport.removeStyleName("okStatus");
		        valUCopyPassport.addStyleName("loadStatus");
		        //valUCopyPassport.setValue("Загрузка файла \"" + receiver.getFileName() + "\"");
		        valUCopyPassport.setVisible(true);
		        valUCopyPassport.setValue("<div>Загрузка файла</div>");*/
		    }
		
		});
		
		uCopyPassport.addListener(new Upload.SucceededListener() {
		    public void uploadSucceeded(SucceededEvent event) {
		    	uploadFiles.add(myReceiver.getReceiveFile());
		    	uCopyPassport.setImmediate(true);
		    	uCopyPassport.setEnabled(true);
		    	uCopyPassport.requestRepaint();
		    	bSend.setEnabled(true);
		    	
		    	//int countFiles = maxFiles - ((MyReceiver)uCopyPassport.getReceiver()).getCountFilesDirOfReceiveFile();
		    	int countFiles = maxFiles - uploadFiles.size();
		    	String str = "файлов";
		    	if (countFiles == 0) {
		    		valCounterFilesPassport.setValue("Вы не можете больше добавлять файлы");
		    		uCopyPassport.setEnabled(false);
		    	}
		    	if (countFiles == 1) str = "файл";
		    	if (countFiles >= 2 && countFiles <= 4) str = "файла";
		    	if (countFiles >= 5) str = "файлов";
		    	
		    	if (countFiles >= 1 && countFiles <= maxFiles) valCounterFilesPassport.setValue(new StringBuffer("Вы можете добавить еще ").append(countFiles).append(" ").append(str));
		    	
		        // This method gets called when the uCopyPassport finished successfully
		    	valUCopyPassport.removeStyleName("validator");
		        valUCopyPassport.removeStyleName("loadStatus");
		        valUCopyPassport.addStyleName("okStatus");
		        valUCopyPassport.setVisible(true);
		        valUCopyPassport.setValue("");
		        
		    	filesUCopyPassport.addStyleName("okStatus");
		        filesUCopyPassport.setVisible(true);
		        if (filesUCopyPassport.getValue() == null) filesUCopyPassport.setValue("");
		        else filesUCopyPassport.setValue(filesUCopyPassport.getValue() + "<div>Загружен файл " + event.getFilename() + "</div>");
		        
		        bCancelUploadPassport.setVisible(true);
		    	//bCancelUpload.setCaption("Удалить последний файл");
		    	//myRefresher.stopRefresh();
		    	//uploadRefresher.stopRefresh();
				
		    }
		});
		
		uCopyPassport.addListener(new Upload.FailedListener() {
		    public void uploadFailed(FailedEvent event) {
		    	uCopyPassport.setImmediate(true);
		    	bSend.setEnabled(true);
		        // This method gets called when the uCopyPassport failed
		    	valUCopyPassport.removeStyleName("okStatus");
		        valUCopyPassport.removeStyleName("loadStatus");
		        valUCopyPassport.addStyleName("validator");
		        valUCopyPassport.setVisible(true);
		    	valUCopyPassport.setValue("<div>" + valUCopyPassport.getData() + "</div>");

		    	//удалить папку с файлами аплоада
		    	//((MyReceiver)uCopyPassport.getReceiver()).deleteDirOfReceiveFile();
		    	((MyReceiver)uCopyPassport.getReceiver()).deleteReceiveFile();
		    	
		    	//если есть файлы, то переименовать кнопку
		    	//if (((MyReceiver)uCopyPassport.getReceiver()).getCountFilesDirOfReceiveFile() > 0) {
		    	if (uploadFiles.size() > 0) {
		    		//bCancelUpload.setCaption("Удалить последний файл");
		    		bCancelUploadPassport.setVisible(true);
		    	} else {
		    		//bCancelUpload.setCaption("Отмена");
		    		bCancelUploadPassport.setVisible(false);
		    	}
		    	
		    	//bCancelUpload.setVisible(false);
		    	//myRefresher.stopRefresh();
				//uCopyPassport.setEnabled(true);
		    	//uploadRefresher.stopRefresh();
		    }
		});
		
		uCopyPassport.addListener(new Upload.FinishedListener() {
		    public void uploadFinished(FinishedEvent event) {
		    	uCopyPassport.setImmediate(true);
		        // This method gets called always when the uCopyPassport finished,
				// either succeeding or failing
				//uCopyPassport.setVisible(true);
				//uCopyPassport.setCaption("Select another file");
		    	//bCancelUpload.setCaption("Удалить последний файл");
		    }
		
		});
		
		
		//Слушать закрытия окна, удалить файл
		addListener(new CloseListener() {
			
			public void windowClose(CloseEvent e) {
				//удалить папку с файлами аплоада
				((MyReceiver)uCopyPassport.getReceiver()).deleteDirOfReceiveFile();
				valUCopyPassport.setVisible(false);
				valUCopyPassport.setValue(null);
			}
		});
		
		String str = "файлов";
    	if (maxFiles == 1) str = "файл";
    	if (maxFiles >= 2 && maxFiles <= 4) str = "файла";
    	if (maxFiles >= 5) str = "файлов";
		
		valCounterFilesPassport = new Label("Вы можете добавить еще " + String.valueOf(maxFiles) + " " + str,Label.CONTENT_XHTML);
		valCounterFilesPassport.addStyleName("valCounterFilesPassport");
		valCounterFilesPassport.addStyleName("validator");
		valCounterFilesPassport.setVisible(true);
		
		valUCopyPassport = new Label("",Label.CONTENT_XHTML);
		valUCopyPassport.addStyleName("valUCopyPassport");
		valUCopyPassport.addStyleName("validator");
		valUCopyPassport.setVisible(false);
		
		filesUCopyPassport = new Label("",Label.CONTENT_XHTML);
		filesUCopyPassport.addStyleName("valUCopyPassport");
		filesUCopyPassport.addStyleName("validator");
		filesUCopyPassport.setVisible(false);
		
		culIndividual.addComponent(bCancelUploadPassport, "bCancelUploadPassport");
		//culIndividual.addComponent(bDeleteUploadPassport, "bDeleteUploadPassport");
		culIndividual.addComponent(valCounterFilesPassport, "valCounterFilesPassport");
		culIndividual.addComponent(valUCopyPassport, "valUCopyPassport");
		culIndividual.addComponent(filesUCopyPassport, "filesUCopyPassport");
		culIndividual.addComponent(uCopyPassport, "uCopyPassport");
		
	}
	
}
