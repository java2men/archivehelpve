package ru.admomsk.uikt.view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;

import ru.admomsk.uikt.utils.MyFileUtils;

import com.liferay.portal.kernel.util.FileUtil;
import com.vaadin.ui.Upload.Receiver;

class MyReceiver implements Receiver {
	
	private static final long serialVersionUID = -645883517791763199L;
	
	private File uploadDir;
    private File receiveFile;
    
    public OutputStream receiveUpload(String filename, String MIMEType) {
    	//System.out.println("receiveUpload");
    	//System.out.println("java.io.tmpdir="+System.getProperty("java.io.tmpdir"));
        
        FileOutputStream fos = null;
        
        try {
        	if (uploadDir == null) { 
            	File tempFile = File.createTempFile("archive_help_ve_request", "");
        		//Получить path временного файла
        		String pathTempFile = tempFile.getAbsolutePath();
        		//Удалить временный файл
        		org.apache.commons.io.FileUtils.forceDelete(tempFile);
        		//создать директорию на основе path временного файла
        		uploadDir = new File(pathTempFile);
        		org.apache.commons.io.FileUtils.forceMkdir(uploadDir);
        	}
        	/*int i = 1;
        	if (uploadDir.listFiles() != null ) {
        		if (uploadDir.listFiles().length != 0) {
        			i = uploadDir.listFiles().length + 1;
        		}
        	}*/
        	String latinFilename = "file_upload" + String.valueOf(getCountFilesDirOfReceiveFile() + 1) + "_";
        	//receiveFile = File.createTempFile(FileUtils.getFileName(filename), FileUtils.getExt(filename), uploadDir);
        	receiveFile = File.createTempFile(latinFilename, MyFileUtils.getExt(filename), uploadDir);
			fos = new FileOutputStream(receiveFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return fos;
    }
    
    public File getReceiveFile() {
        return receiveFile;
    }
    
    private File getLastReceiveFile() {
    	File lastReceiveFile;
    	if (getCountFilesDirOfReceiveFile() == 0) {
    		lastReceiveFile = null;
    	} else {
    		lastReceiveFile = uploadDir.listFiles()[uploadDir.listFiles().length - 1];
    	}
        return lastReceiveFile;
    }
    
    public void deleteLastReceiveFile() {
    	File lastReceiveFile = getLastReceiveFile();
    	try {
    		if (lastReceiveFile != null) {
    			if (lastReceiveFile.getParentFile() != null) {
    				if (getCountFilesDirOfReceiveFile() == 1) {
    					deleteDirOfReceiveFile();
    				} else {
        				if (!FileUtil.delete(lastReceiveFile)) {
        					FileUtils.deleteQuietly(lastReceiveFile);
        					FileUtils.forceDelete(lastReceiveFile);
        				}
        				lastReceiveFile = null;
    				}
    			}
    		}
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public File getDirOfReceiveFile() {
        return uploadDir;
    }
    
    public int getCountFilesDirOfReceiveFile() {
    	if (uploadDir == null) return 0;
    	if (uploadDir.listFiles() == null) return 0;
    	return uploadDir.listFiles().length;
    }
    
    public void deleteAnyFile(File file) {
    	
    	if (file != null) {
    		try {
    			if (file.exists()) FileUtils.forceDelete(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
    	
    	if (file != null) {
    		if (file.exists()) FileUtil.delete(file);
    	}
    	if (file != null) {
    		if (file.exists()) FileUtils.deleteQuietly(file);
    	}
    }
    
    public void deleteReceiveFile() {
    	try {
    		if (receiveFile != null) {
    			if (receiveFile.getParentFile() != null) {
    				//org.apache.commons.io.FileUtils.deleteDirectory(receiveFile);
    				if (!FileUtil.delete(receiveFile)) {
    					if (receiveFile.exists()) FileUtils.deleteQuietly(receiveFile);
    					if (receiveFile.exists()) FileUtils.forceDelete(receiveFile);
    				}
    				//org.apache.commons.io.FileUtils.deleteQuietly(arg0)
    				receiveFile = null;
    			}
    		}
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    public void deleteDirOfReceiveFile() {
    	try {
    		if (uploadDir != null) {
    			if (uploadDir.exists()) FileUtils.deleteDirectory(uploadDir);
    			if (uploadDir.exists()) FileUtil.deltree(uploadDir);
    		}
    		uploadDir = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
